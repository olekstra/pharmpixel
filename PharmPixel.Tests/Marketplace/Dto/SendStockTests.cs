﻿namespace PharmPixel.Marketplace.Dto
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class SendStockTests
    {
        [Fact]
        public void RequestSerializedOk()
        {
            var obj = new SendStockRequest
            {
                Data = new System.Collections.Generic.List<SendStockRequest.Item>()
                {
                    new SendStockRequest.Item { Manufacturer = "string1", Name = "string2", Provider = "string3", Stock = 4 },
                    new SendStockRequest.Item { Manufacturer = "string4", Name = "string5", Provider = "string6", Stock = 11.22M },
                },
            };

            var actualText = JsonConvert.SerializeObject(obj, MarketplaceOptions.JsonSerializerSettings);

            var expectedJson = JObject.Parse(Samples.SendStockRequest);
            var expectedTextWithoutFormatting = JsonConvert.SerializeObject(expectedJson, Formatting.None);

            var actualJson = JObject.Parse(actualText);

            Assert.Equal(expectedTextWithoutFormatting, actualText);
            Assert.True(JToken.DeepEquals(expectedJson, actualJson));
        }

        [Fact]
        public void ResponseDeserializedOk()
        {
            var obj = JsonConvert.DeserializeObject<SendStockResponse>(Samples.SendStockResponse, MarketplaceOptions.JsonSerializerSettings);

            Assert.NotNull(obj);

            Assert.NotNull(obj.Data);
            Assert.Equal("string1", obj.Data.Status);

            Assert.Equal("string2", obj.Message);
            Assert.Equal(111, obj.Status);
        }
    }
}
