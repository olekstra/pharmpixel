﻿namespace PharmPixel.Marketplace.Dto
{
    using System;
    using Newtonsoft.Json;
    using Xunit;

    public class GetMatchesTests
    {
        [Fact]
        public void ResponseDeserializedOk()
        {
            var obj = JsonConvert.DeserializeObject<GetMatchesResponse>(Samples.GetMatchesResponse, MarketplaceOptions.JsonSerializerSettings);

            Assert.NotNull(obj);

            Assert.NotNull(obj.Data);
            Assert.Equal(2, obj.Data.Count);

            Assert.True(obj.Data[0].Export);
            Assert.Equal("string1", obj.Data[0].Id);
            Assert.Equal("string2", obj.Data[0].Manufacturer);
            Assert.Equal("string3", obj.Data[0].Name);

            Assert.False(obj.Data[1].Export);
            Assert.Equal("string4", obj.Data[1].Id);
            Assert.Equal("string5", obj.Data[1].Manufacturer);
            Assert.Equal("string6", obj.Data[1].Name);

            Assert.Equal("string7", obj.Message);
            Assert.Equal(999, obj.Status);
        }
    }
}
