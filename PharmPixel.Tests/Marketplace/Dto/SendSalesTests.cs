﻿namespace PharmPixel.Marketplace.Dto
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class SendSalesTests
    {
        [Fact]
        public void ValidRequestSerializedOk()
        {
            var obj = new SendSalesValidRequest
            {
                Data = new System.Collections.Generic.List<SendSalesValidRequest.Item>()
                {
                    new SendSalesValidRequest.Item
                    {
                        CashTotalSum = 123.45M,
                        CreatedAt = new DateTimeOffset(2019, 7, 12, 14, 11, 11, TimeSpan.FromHours(3)),
                        DateTime = new DateTimeOffset(2019, 7, 12, 10, 12, 12, TimeSpan.FromHours(-2)),
                        ECashTotalSum = 234.56M,
                        FiscalDocumentNumber = 45678,
                        FiscalDriveNumber = "string1",
                        Inn = "string2",
                        KktRegId = "string3",
                        Link = "string4",
                        Name = "string5",
                        NdsTotalSum = 6.66M,
                        Ofd = "string7",
                        OperationType = 8,
                        Price = 99.999M,
                        PriceSellIn = 101.01M,
                        ProductId = "string11",
                        Quantity = 12M,
                        TaxationType = 13,
                        Total = 1414.14M,
                        TotalSum = 1515.15M,
                        UpdatedAt = new DateTimeOffset(2019, 7, 12, 14, 13, 13, TimeSpan.FromHours(1)),
                    },
                },
            };

            var actualText = JsonConvert.SerializeObject(obj, MarketplaceOptions.JsonSerializerSettings);

            var expectedJson = JObject.Parse(Samples.SendSalesValidRequest);
            var expectedTextWithoutFormatting = JsonConvert.SerializeObject(expectedJson, Formatting.None);

            var actualJson = JObject.Parse(actualText);

            Assert.Equal(expectedTextWithoutFormatting, actualText);
            Assert.True(JToken.DeepEquals(expectedJson, actualJson));
        }

        [Fact]
        public void InvalidRequestSerializedOk()
        {
            var obj = new SendSalesInvalidRequest
            {
                Data = new System.Collections.Generic.List<SendSalesInvalidRequest.Item>()
                {
                    new SendSalesInvalidRequest.Item
                    {
                        Name = "string1",
                        ProductId = "string2",
                        Manufacturer = "string3",
                        Provider = "string4",
                        PriceSellOut = 55.55M,
                        PriceSellIn = 6.66M,
                        Quantity = 7,
                        DatePay = new DateTimeOffset(2019, 7, 12, 13, 13, 13, TimeSpan.Zero),
                        Number = "string9",
                    },
                },
            };

            var actualText = JsonConvert.SerializeObject(obj, MarketplaceOptions.JsonSerializerSettings);

            var expectedJson = JObject.Parse(Samples.SendSalesInvalidRequest);
            var expectedTextWithoutFormatting = JsonConvert.SerializeObject(expectedJson, Formatting.None);

            var actualJson = JObject.Parse(actualText);

            Assert.Equal(expectedTextWithoutFormatting, actualText);
            Assert.True(JToken.DeepEquals(expectedJson, actualJson));
        }

        [Fact]
        public void ResponseDeserializedOk()
        {
            var obj = JsonConvert.DeserializeObject<SendSalesResponse>(Samples.SendSalesResponse, MarketplaceOptions.JsonSerializerSettings);

            Assert.NotNull(obj);

            Assert.NotNull(obj.Data);
            Assert.Equal("string1", obj.Data.Status);

            Assert.Equal("string2", obj.Message);
            Assert.Equal(777, obj.Status);
        }
    }
}
