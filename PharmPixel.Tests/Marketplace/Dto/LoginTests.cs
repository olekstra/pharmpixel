﻿namespace PharmPixel.Marketplace.Dto
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class LoginTests
    {
        [Fact]
        public void RequestSerializedOk()
        {
            var obj = new LoginRequest { Email = "string1", Password = "string2" };

            var actualText = JsonConvert.SerializeObject(obj, MarketplaceOptions.JsonSerializerSettings);

            var expectedJson = JObject.Parse(Samples.LoginRequest);
            var expectedTextWithoutFormatting = JsonConvert.SerializeObject(expectedJson, Formatting.None);

            var actualJson = JObject.Parse(actualText);

            Assert.True(JToken.DeepEquals(expectedJson, actualJson));
            Assert.Equal(expectedTextWithoutFormatting, actualText);
        }

        [Fact]
        public void ResponseDeserializedOk()
        {
            var obj = JsonConvert.DeserializeObject<LoginResponse>(Samples.LoginResponse, MarketplaceOptions.JsonSerializerSettings);

            Assert.NotNull(obj);

            Assert.NotNull(obj.Data);
            Assert.Equal("string1", obj.Data.AccessToken);
            Assert.Equal("string2", obj.Data.RefreshToken);
            Assert.Equal("string3", obj.Data.UserId);

            Assert.Equal("string4", obj.Message);
            Assert.Equal(555, obj.Status);
        }
    }
}
