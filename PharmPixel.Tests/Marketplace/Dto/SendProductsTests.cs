﻿namespace PharmPixel.Marketplace.Dto
{
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class SendProductsTests
    {
        [Fact]
        public void RequestSerializedOk()
        {
            var obj = new SendProductsRequest
            {
                Data = new System.Collections.Generic.List<SendProductsRequest.Item>()
                {
                    new SendProductsRequest.Item { Manufacturer = "string1", Name = "string2" },
                    new SendProductsRequest.Item { Manufacturer = "string3", Name = "string4" },
                },
            };

            var actualText = JsonConvert.SerializeObject(obj, MarketplaceOptions.JsonSerializerSettings);

            var expectedJson = JObject.Parse(Samples.SendProductsRequest);
            var expectedTextWithoutFormatting = JsonConvert.SerializeObject(expectedJson, Formatting.None);

            var actualJson = JObject.Parse(actualText);

            Assert.True(JToken.DeepEquals(expectedJson, actualJson));
            Assert.Equal(expectedTextWithoutFormatting, actualText);
        }

        [Fact]
        public void ResponseDeserializedOk()
        {
            var obj = JsonConvert.DeserializeObject<SendProductsResponse>(Samples.SendProductsResponse, MarketplaceOptions.JsonSerializerSettings);

            Assert.NotNull(obj);

            Assert.NotNull(obj.Data);
            Assert.Equal("string1", obj.Data.Status);

            Assert.Equal("string2", obj.Message);
            Assert.Equal(777, obj.Status);
        }
    }
}
