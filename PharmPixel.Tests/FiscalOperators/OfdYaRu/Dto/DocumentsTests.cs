﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class DocumentsTests
    {
        [Fact]
        public void RequestSerializedOk()
        {
            var obj = new DocumentsRequest("9281000100000111", new DateTime(2019, 7, 22))
            {
                Start = 1,
                End = 2500,
            };

            var actualText = JsonConvert.SerializeObject(obj, OfdYaRuOptions.JsonSerializerSettings);

            var expectedJson = JObject.Parse(Samples.DocumentsRequest);
            var expectedTextWithoutFormatting = JsonConvert.SerializeObject(expectedJson, Formatting.None);

            var actualJson = JObject.Parse(actualText);

            Assert.True(JToken.DeepEquals(expectedJson, actualJson));
            Assert.Equal(expectedTextWithoutFormatting, actualText);
        }

        [Fact]
        public void ResponseDeserializedOk()
        {
            var obj = JsonConvert.DeserializeObject<DocumentsResponse>(Samples.DocumentsResponse, OfdYaRuOptions.JsonSerializerSettings);

            Assert.NotNull(obj);

            Assert.Equal(2, obj.Count);
            Assert.NotNull(obj.Items);
            Assert.Equal(2, obj.Items.Count);

            var doc = obj.Items[0];
            Assert.NotNull(doc);

            Assert.Equal(new DateTimeOffset(2019, 7, 26, 9, 1, 0, TimeSpan.Zero), doc.DateTime);
            Assert.Equal(10M, doc.ProvisionSum);
            Assert.Equal(2, doc.FiscalDocumentFormatVer);
            Assert.Equal(3, doc.Code);
            Assert.Equal("9281022222180009", doc.FiscalDriveNumber);
            Assert.Equal(385, doc.ShiftNumber);
            Assert.Equal(new DateTimeOffset(2019, 7, 26, 6, 1, 1, TimeSpan.Zero), doc.ReceivingDate);
            Assert.Equal("Шорохова Т. Н.", doc.Operator);
            Assert.Equal(1, doc.RequestNumber);
            Assert.Equal(77M, doc.EcashTotalSum);
            Assert.Equal(36188, doc.FiscalDocumentNumber);
            Assert.Equal(44, doc.TaxationType);
            Assert.Equal(22000M, doc.NdsNo);
            Assert.Equal("7715620000", doc.UserInn);
            Assert.Equal(99M, doc.CreditSum);
            Assert.Equal("0000548000061110", doc.KktRegId);
            Assert.Equal(22000M, doc.CashTotalSum);
            Assert.Equal(26000M, doc.TotalSum);
            Assert.Equal("www.nalog.ru", doc.AuthorityUri);
            Assert.Equal("127543, г. Москва, ул. Лескова, д.", doc.RetailAddress);
            Assert.Equal("285345829425", doc.FiscalSign);
            Assert.Equal(12, doc.OperationType);
            Assert.Equal(1335M, doc.PrepaidSum);

            Assert.Single(doc.Items);

            var item = doc.Items[0];
            Assert.NotNull(item);

            Assert.Equal(1.0M, item.Quantity);
            Assert.Equal(22600M, item.Price);
            Assert.Equal("Хартман бинт Peha-Haft фиксир.когези", item.Name);
            Assert.Equal(55500M, item.Sum);
            Assert.Equal(1, item.ProductType);
            Assert.Equal(4, item.PaymentType);
        }
    }
}
