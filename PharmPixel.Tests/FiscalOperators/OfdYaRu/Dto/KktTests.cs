﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class KktTests
    {
        [Fact]
        public void RequestSerializedOk()
        {
            var obj = new KktRequest(new DateTime(2017, 10, 25));

            var actualText = JsonConvert.SerializeObject(obj, OfdYaRuOptions.JsonSerializerSettings);

            var expectedJson = JObject.Parse(Samples.KktRequest);
            var expectedTextWithoutFormatting = JsonConvert.SerializeObject(expectedJson, Formatting.None);

            var actualJson = JObject.Parse(actualText);

            Assert.True(JToken.DeepEquals(expectedJson, actualJson));
            Assert.Equal(expectedTextWithoutFormatting, actualText);
        }

        [Fact]
        public void ResponseDeserializedOk()
        {
            var obj = JsonConvert.DeserializeObject<KktResponse>(Samples.KktResponse, OfdYaRuOptions.JsonSerializerSettings);

            Assert.NotNull(obj);

            Assert.Equal(2, obj.Count);
            Assert.NotNull(obj.Kkt);
            Assert.Equal(2, obj.Kkt.Count);

            Assert.True(obj.Kkt.TryGetValue("9281000100180080", out var kkt));
            Assert.NotNull(kkt);
            Assert.Equal("127543, г. Москва, ул. 1А", kkt.Address);
            Assert.Equal("2019-07-26 10:03:00", kkt.Last);
            Assert.Equal("0000548000060000", kkt.Kktregid);
            Assert.Equal(120800M, kkt.Turnover);
            Assert.Equal(8, kkt.ReceiptCount);

            Assert.True(obj.Kkt.TryGetValue("8710000100770020", out kkt));
            Assert.NotNull(kkt);
            Assert.Equal("127543, г. Москва, ул. д. стр. 2", kkt.Address);
            Assert.Equal("-/-/-", kkt.Last);
            Assert.Equal("000054111060110", kkt.Kktregid);
            Assert.Equal(0M, kkt.Turnover);
            Assert.Equal(0, kkt.ReceiptCount);
        }
    }
}
