﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    using System;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using Xunit;

    public class GetChequeLinkTests
    {
        [Fact]
        public void RequestSerializedOk()
        {
            var obj = new GetChequeLinkRequest("0012300045600789", 12345);

            var actualText = JsonConvert.SerializeObject(obj, OfdYaRuOptions.JsonSerializerSettings);

            var expectedJson = JObject.Parse(Samples.GetChequeLinkRequest);
            var expectedTextWithoutFormatting = JsonConvert.SerializeObject(expectedJson, Formatting.None);

            var actualJson = JObject.Parse(actualText);

            Assert.True(JToken.DeepEquals(expectedJson, actualJson));
            Assert.Equal(expectedTextWithoutFormatting, actualText);
        }

        [Fact]
        public void ResponseDeserializedOk()
        {
            var obj = JsonConvert.DeserializeObject<GetChequeLinkResponse>(Samples.GetChequeLinkResponse, OfdYaRuOptions.JsonSerializerSettings);

            Assert.NotNull(obj);
            Assert.Equal("receipt.2019-07", obj.DocType);
            Assert.Equal("https://ofd-ya.ru/r?fpCn0PcfEQlpMQ", obj.Link);

            Assert.NotNull(obj.FiscalDocument);
            Assert.Equal("9281000100189589", obj.FiscalDocument.FiscalDriveNumber);
            Assert.Equal(36188, obj.FiscalDocument.FiscalDocumentNumber);
            //// Остальные поля не будем проверять, проверяли в /documents
        }
    }
}
