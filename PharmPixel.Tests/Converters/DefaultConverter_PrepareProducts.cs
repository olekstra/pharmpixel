﻿namespace PharmPixel.Converters
{
    using System;
    using System.Collections.Generic;
    using Xunit;

    public class DefaultConverter_PrepareProducts
    {
        [Fact]
        public void DataIsDeduplicated()
        {
            var input = new List<(string, string)>()
            {
                ("string1", "string2"),
                ("string1", "string2"), // should be removed (equal to 1st)
                ("STRING1", "STRING2"), // should be removed too (equal to 1st ignoring case)
                ("string1", "string3"),
                ("string4", "string2"),
            };

            var output = DefaultConverter.PrepareProducts(input);

            Assert.NotNull(output);
            Assert.Equal(3, output.Count);

            Assert.Equal("string1", output[0].Manufacturer);
            Assert.Equal("string2", output[0].Name);

            Assert.Equal("string1", output[1].Manufacturer);
            Assert.Equal("string3", output[1].Name);

            Assert.Equal("string4", output[2].Manufacturer);
            Assert.Equal("string2", output[2].Name);
        }
    }
}
