﻿namespace PharmPixel.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Moq;
    using PharmPixel.Marketplace;
    using Xunit;

    public class DefaultConverter_PrepareStockAsync
    {
        private Mock<IMarketplaceService> marketplaceServiceMock;

        public DefaultConverter_PrepareStockAsync()
        {
            marketplaceServiceMock = new Mock<IMarketplaceService>(MockBehavior.Strict);
        }

        [Fact]
        public async Task PerformFiltering()
        {
            var matches = new List<(string, string, string)>()
            {
                ("string1", "string2", "1"),
                ("string3", "string4", "2"),
            };

            marketplaceServiceMock
                .Setup(x => x.GetExportableMappingsAsync())
                .ReturnsAsync(matches);

            var input = new List<(string, string,  string, decimal)>()
            {
                ("string3", "string4", "-", 11), // match!
                ("string3", "string0", "-", 22), // 'name' does not match
                ("string0", "string4", "-", 33), // 'manufacturer' does not match
            };

            var output = await DefaultConverter.PrepareStockAsync(input, marketplaceServiceMock.Object);

            Assert.Single(output);
            Assert.Equal("string3", output[0].Manufacturer);
            Assert.Equal("string4", output[0].Name);
            Assert.Equal("-", output[0].Provider);
            Assert.Equal(11, output[0].Stock);
        }

        [Fact]
        public async Task DataIsGrouped()
        {
            var matches = new List<(string, string, string)>()
            {
                ("string1", "string2", "1"),
                ("string3", "string4", "2"),
            };

            marketplaceServiceMock
                .Setup(x => x.GetExportableMappingsAsync())
                .ReturnsAsync(matches);

            var input = new List<(string, string, string, decimal)>()
            {
                // group1
                ("string1", "string2", "a", 1),
                ("string1", "string2", "a", 2),

                // group 2 (different provider)
                ("string1", "string2", "b", 7),

                // group 3 (case different)
                ("string3", "string4", "a", 10),
                ("STRING3", "StRiNg4", "a", 11),
            };

            var output = await DefaultConverter.PrepareStockAsync(input, marketplaceServiceMock.Object);

            Assert.NotNull(output);
            Assert.Equal(3, output.Count);

            Assert.Equal("string1", output[0].Manufacturer);
            Assert.Equal("string2", output[0].Name);
            Assert.Equal("a", output[0].Provider);
            Assert.Equal(3, output[0].Stock);

            Assert.Equal("string1", output[1].Manufacturer);
            Assert.Equal("string2", output[1].Name);
            Assert.Equal("b", output[1].Provider);
            Assert.Equal(7, output[1].Stock);

            Assert.Equal("string3", output[2].Manufacturer);
            Assert.Equal("string4", output[2].Name);
            Assert.Equal("a", output[2].Provider);
            Assert.Equal(21, output[2].Stock);
        }
    }
}
