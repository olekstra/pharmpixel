﻿namespace PharmPixel.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Moq;
    using PharmPixel.FiscalOperators;
    using PharmPixel.Marketplace;
    using Xunit;

    public class DefaultConverter_PrepareSalesAsync
    {
        private static readonly DefaultConverter.SaleData Sale1 = new DefaultConverter.SaleData
        {
            Name = "name1",
            Manufacturer = "manufr1",
            Provider = "prov1",
            Number = "111",
            PriceSellIn = 11,
            PriceSellOut = 1111,
            Quantity = 1,
            DateTime = new DateTime(2019, 7, 30, 11, 11, 11),
        };

        private static readonly DefaultConverter.SaleData Sale2 = new DefaultConverter.SaleData
        {
            Name = "name2",
            Manufacturer = "manufr2",
            Provider = "prov2",
            Number = "222",
            PriceSellIn = 22,
            PriceSellOut = 2222,
            Quantity = 2,
            DateTime = new DateTime(2019, 7, 30, 22, 22, 22),
        };

        private static readonly DefaultConverter.SaleData Sale3 = new DefaultConverter.SaleData
        {
            Name = "name3",
            Manufacturer = "manufr3",
            Provider = "prov3",
            Number = "333",
            PriceSellIn = 33,
            PriceSellOut = 3333,
            Quantity = 3,
            DateTime = new DateTime(2019, 7, 30, 13, 33, 33),
        };

        private static readonly FiscalDocument Fd1 = new FiscalDocument
        {
            Name = Sale1.Name,
            Quantity = Sale1.Quantity,
            FiscalDocumentNumber = long.Parse(Sale1.Number),
            DateTime = new DateTimeOffset(Sale1.DateTime, TimeSpan.Zero),
        };

        private static readonly FiscalDocument Fd3 = new FiscalDocument
        {
            Name = Sale3.Name,
            Quantity = Sale3.Quantity,
            FiscalDocumentNumber = long.Parse(Sale3.Number),
            DateTime = new DateTimeOffset(Sale3.DateTime, TimeSpan.Zero),
        };

        private Mock<IMarketplaceService> marketplaceServiceMock;

        private Mock<IFiscalOperatorService> fiscalOperatorServiceMock;

        public DefaultConverter_PrepareSalesAsync()
        {
            marketplaceServiceMock = new Mock<IMarketplaceService>(MockBehavior.Strict);
            fiscalOperatorServiceMock = new Mock<IFiscalOperatorService>(MockBehavior.Strict);
        }

        [Fact]
        public async Task PerformFiltering()
        {
            var input = new List<DefaultConverter.SaleData> { Sale1, Sale2 };

            var matches = new List<(string, string, string)>()
            {
                (Sale3.Manufacturer, Sale3.Name, "id3"),
            };

            marketplaceServiceMock
                .Setup(x => x.GetExportableMappingsAsync())
                .ReturnsAsync(matches)
                .Verifiable();

            var (valid, invalid) = await DefaultConverter.PrepareSalesAsync(input, marketplaceServiceMock.Object, fiscalOperatorServiceMock.Object, false);

            Assert.Empty(valid);
            Assert.Empty(invalid);

            marketplaceServiceMock.Verify();
        }

        [Fact]
        public async Task PerformGroupAndCompare()
        {
            var input = new List<DefaultConverter.SaleData> { Sale1, Sale2 };

            var fiscal = new List<FiscalDocument> { Fd1, Fd3 };

            var matches = new List<(string, string, string)>()
            {
                (Sale1.Manufacturer, Sale1.Name, "id1"),
                (Sale2.Manufacturer, Sale2.Name, "id2"),
            };

            marketplaceServiceMock
                .Setup(x => x.GetExportableMappingsAsync())
                .ReturnsAsync(matches)
                .Verifiable();

            fiscalOperatorServiceMock
                .Setup(x => x.GetLinkAsync(It.IsAny<FiscalDocument>()))
                .ReturnsAsync((FiscalDocument fd) => "link" + fd.FiscalDocumentNumber);

            fiscalOperatorServiceMock
                .Setup(x => x.GetFiscalDocumentsAsync(new DateTime(2019, 7, 30)))
                .ReturnsAsync(fiscal)
                .Verifiable();

            var (valid, invalid) = await DefaultConverter.PrepareSalesAsync(input, marketplaceServiceMock.Object, fiscalOperatorServiceMock.Object, false);

            Assert.Single(valid);
            Assert.Equal("id1", valid[0].ProductId);
            Assert.Equal("link111", valid[0].Link);

            Assert.Single(invalid);
            Assert.Equal("id2", invalid[0].ProductId);

            marketplaceServiceMock.Verify();
            fiscalOperatorServiceMock.Verify();
        }
    }
}
