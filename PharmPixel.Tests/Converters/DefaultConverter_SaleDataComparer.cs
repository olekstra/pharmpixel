﻿namespace PharmPixel.Converters
{
    using System;
    using System.Collections.Generic;
    using Xunit;

    public class DefaultConverter_SaleDataComparer
    {
        private DefaultConverter.SaleDataComparer comparer = new DefaultConverter.SaleDataComparer();

        /// <summary>
        /// Сверка по наименованию, количество и если есть номер - по номеру (не глядя на дату).
        /// </summary>
        [Fact]
        public void NameQuantityNumber()
        {
            var one = ("Some Name", 1, new DateTime(2019, 7, 30, 3, 5, 7), "123");
            var two = ("sOME nAME", 1, new DateTime(2010, 11, 3, 7, 5, 3), "123");

            Assert.True(comparer.Equals(one, two));
            Assert.Equal(comparer.GetHashCode(one), comparer.GetHashCode(two));
        }

        /// <summary>
        /// То же что и <see cref="NameQuantityNumber"/>, но одно наименование обрезано.
        /// </summary>
        [Fact]
        public void NameQuantityNumberWithTrimmedName()
        {
            var one = ("Some Na", 1, new DateTime(2019, 7, 30, 3, 5, 7), "123");
            var two = ("sOME nAME", 1, new DateTime(2010, 11, 3, 7, 5, 3), "123");

            Assert.True(comparer.Equals(one, two));
            Assert.Equal(comparer.GetHashCode(one), comparer.GetHashCode(two));
        }

        /// <summary>
        /// Сверка по наименованию, количество и если есть номер - проверка что НЕ равны.
        /// </summary>
        [Fact]
        public void NameQuantityNumberMismatch()
        {
            var dt = new DateTime(2019, 7, 30, 3, 5, 7);

            var objSample = ("Some Name", 1, dt, "123");
            var objOtherName = ("Other Name", 1, dt, "123");
            var objOtherQuantity = ("Some Name", 2, dt, "123");
            var objOtherNumber = ("Some Name", 1, dt, "12345");

            Assert.False(comparer.Equals(objSample, objOtherName));
            Assert.False(comparer.Equals(objSample, objOtherQuantity));
            Assert.False(comparer.Equals(objSample, objOtherNumber));
        }

        /// <summary>
        /// При отсутствии номера - учитывается время, разница не более Х.
        /// </summary>
        [Fact]
        public void NameQuantityTime()
        {
            var bias = TimeSpan.FromSeconds(11);
            DefaultConverter.MaxAllowedBias = bias;

            var dt = new DateTime(2019, 7, 30, 3, 5, 7);

            var obj1 = ("Some Name", 1, dt, "123");
            var obj2 = ("Some Name", 1, dt, default(string));
            var obj3 = ("Some Name", 1, dt.Add(bias), default(string));
            var obj4 = ("Some Name", 1, dt.Subtract(bias), default(string));
            var obj5 = ("Some Name", 1, dt.Add(bias).Add(bias), default(string));

            Assert.True(comparer.Equals(obj1, obj2));
            Assert.Equal(comparer.GetHashCode(obj1), comparer.GetHashCode(obj2));

            Assert.True(comparer.Equals(obj1, obj3));
            Assert.Equal(comparer.GetHashCode(obj1), comparer.GetHashCode(obj3));

            Assert.True(comparer.Equals(obj1, obj4));
            Assert.Equal(comparer.GetHashCode(obj1), comparer.GetHashCode(obj4));

            Assert.False(comparer.Equals(obj1, obj5));
        }
    }
}
