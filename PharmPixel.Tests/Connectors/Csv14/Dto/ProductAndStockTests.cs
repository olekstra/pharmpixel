﻿namespace PharmPixel.Connectors.Csv14.Dto
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Xunit;

    public class ProductAndStockTests
    {
        [Fact]
        public void ReadValidFile()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            var ass = this.GetType().Assembly;
            using (var file = new StreamReader(ass.GetManifestResourceStream("PharmPixel.Connectors.Csv14.Dto.sample_ost.txt")))
            {
                using (var csv = new CsvHelper.CsvReader(file, Csv14Options.CsvConfiguration))
                {
                    var list = csv.GetRecords<ProductAndStock>().ToList();

                    Assert.NotNull(list);
                    Assert.Equal(2, list.Count);

                    Assert.Equal("Д-ПАНТЕНОЛ-НИЖФАРМ 5% 30,0 МАЗЬ Д/НАРУЖ ПРИМ", list[0].Name);
                    Assert.Equal("1-2Драй Б.В.", list[0].Manufacturer);
                    Assert.Equal("Катрен ЗАО", list[0].Provider);
                    Assert.Equal(111M, list[0].Stock);
                    Assert.Equal(new DateTime(2019, 2, 1), list[0].Date);

                    Assert.Equal("ШПРИЦ 5МЛ 2-Х КОМП С ИГЛОЙ 22G N100/ИМПОРТ/SFM", list[1].Name);
                    Assert.Equal("ДС ООО для Премиум-фарма ООО", list[1].Manufacturer);
                    Assert.Equal("Катрен ЗАО", list[1].Provider);
                    Assert.Equal(0.29M, list[1].Stock);
                    Assert.Equal(new DateTime(2019, 2, 3), list[1].Date);
                }
            }
        }
    }
}
