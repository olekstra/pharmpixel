﻿namespace PharmPixel.Connectors.Csv14.Dto
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Text;
    using Xunit;

    public class SaleTests
    {
        [Fact]
        public void ReadValidFile()
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            var ass = this.GetType().Assembly;
            using (var file = new StreamReader(ass.GetManifestResourceStream("PharmPixel.Connectors.Csv14.Dto.sample_mov.txt")))
            {
                using (var csv = new CsvHelper.CsvReader(file, Csv14Options.CsvConfiguration))
                {
                    var list = csv.GetRecords<Sale>().ToList();

                    Assert.NotNull(list);
                    Assert.Equal(2, list.Count);

                    Assert.Equal("Фортранс, пор д/р-ра оральн. 64г №4", list[0].Name);
                    Assert.Equal("Beaufour Ipsen Industry", list[0].Manufacturer);
                    Assert.Equal("Протек ЦВ ЗАО", list[0].Provider);
                    Assert.Equal(0.1M, list[0].Quantity);
                    Assert.Equal(316.12M, list[0].PriceSellIn);
                    Assert.Equal(131M, list[0].PriceSellOut);
                    Assert.Equal(new DateTime(2017, 1, 8), list[0].Date);

                    Assert.Equal("Фитосвечи ушные №2", list[1].Name);
                    Assert.Equal("Реамед (г.Самара)", list[1].Manufacturer);
                    Assert.Equal("Катрен ЗАО", list[1].Provider);
                    Assert.Equal(11M, list[1].Quantity);
                    Assert.Equal(27M, list[1].PriceSellIn);
                    Assert.Equal(44M, list[1].PriceSellOut);
                    Assert.Equal(new DateTime(2018, 2, 3), list[1].Date);
                }
            }
        }
    }
}
