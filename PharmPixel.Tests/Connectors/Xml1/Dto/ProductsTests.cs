﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    using System;
    using System.IO;
    using System.Text;
    using Xunit;

    public class ProductsTests
    {
        [Fact]
        public void ReadValidFile()
        {
            var bytes = Encoding.UTF8.GetBytes(Samples.Products1);
            var ms = new MemoryStream(bytes);

            var obj = XmlUtils.Deserialize<Products>(ms);

            Assert.NotNull(obj);
            Assert.NotNull(obj.Product);
            Assert.Equal(2, obj.Product.Length);

            Assert.Equal("Пустырника настойка, фл 25мл", obj.Product[0].Name);
            Assert.Equal("Sanofi", obj.Product[0].Manufacturer);

            Assert.Equal("Пустырника настойка, фл 25мл2", obj.Product[1].Name);
            Assert.Equal("Sanofi2", obj.Product[1].Manufacturer);
        }
    }
}
