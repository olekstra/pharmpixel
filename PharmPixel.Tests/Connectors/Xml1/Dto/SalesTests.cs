﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    using System;
    using System.IO;
    using System.Text;
    using Xunit;

    public class SalesTests
    {
        [Fact]
        public void ReadValidFile()
        {
            var bytes = Encoding.UTF8.GetBytes(Samples.Sales);
            var ms = new MemoryStream(bytes);

            var obj = XmlUtils.Deserialize<Sales>(ms);

            Assert.NotNull(obj);
            Assert.NotNull(obj.Sale);
            Assert.Equal(2, obj.Sale.Length);

            var sale = obj.Sale[0];
            Assert.Equal("Пустырника настойка, фл 25мл", sale.Name);
            Assert.Equal("Sanofi", sale.Manufacturer);
            Assert.Equal("Santens", sale.Provider);
            Assert.Equal(15M, sale.PriceSellOut);
            Assert.Equal(10M, sale.PriceSellIn);
            Assert.Equal(2, sale.Quantity);
            Assert.Equal(new DateTime(2019, 1, 2, 13, 11, 12, DateTimeKind.Unspecified), sale.DatePay);
            Assert.Equal("123", sale.Number);

            sale = obj.Sale[1];
            Assert.Equal("Пустырника настойка, фл 25мл x2", sale.Name);
            Assert.Equal("Sanofi x2", sale.Manufacturer);
            Assert.Equal("Santens x2", sale.Provider);
            Assert.Equal(155M, sale.PriceSellOut);
            Assert.Equal(100M, sale.PriceSellIn);
            Assert.Equal(20, sale.Quantity);
            Assert.Equal(new DateTime(2019, 10, 20, 1, 12, 11, DateTimeKind.Unspecified), sale.DatePay);
            Assert.Equal("12345", sale.Number);
        }
    }
}
