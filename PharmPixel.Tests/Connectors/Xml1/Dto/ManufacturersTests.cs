﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    using System;
    using System.IO;
    using System.Text;
    using Xunit;

    public class ManufacturersTests
    {
        [Fact]
        public void ReadValidFile()
        {
            var bytes = Encoding.UTF8.GetBytes(Samples.Manufacturers1);
            var ms = new MemoryStream(bytes);

            var obj = XmlUtils.Deserialize<Manufacturers>(ms);

            Assert.NotNull(obj);
            Assert.NotNull(obj.Manufacturer);
            Assert.Equal(3, obj.Manufacturer.Length);

            Assert.Equal("Пустырника настойка, фл 25мл1", obj.Manufacturer[0].Name);
            Assert.Equal("Пустырника настойка, фл 25мл2", obj.Manufacturer[1].Name);
            Assert.Equal("Пустырника настойка, фл 25мл3", obj.Manufacturer[2].Name);
        }
    }
}
