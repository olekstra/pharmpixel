﻿namespace PharmPixel
{
    using System;
    using System.IO;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using NLog.Extensions.Logging;
    using PharmPixel.FiscalOperators.OfdYaRu;
    using RecurrentTasks;

    public static class Program
    {
        public static string WorkingDirectory { get; private set; }

        public static async Task Main(string[] args)
        {
            WorkingDirectory = AppContext.BaseDirectory;

            var config = new ConfigurationBuilder()
                 .SetBasePath(WorkingDirectory)
                 .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                 .AddJsonFile("_settings_sample.json", optional: false, reloadOnChange: true)
                 .AddJsonFile("_settings.json", optional: true, reloadOnChange: true)
                 .Build();

            var options = config.Get<ProgramOptions>();

            var serviceBuilder = new ServiceCollection()
                .AddLogging(o => o.AddConfiguration(config.GetSection("Logging")).AddNLog())
                .AddSingleton<IConfiguration>(config)
                .Configure<ProgramOptions>(config)
                .AddTask<Connectors.GetMatchingTask>(o => { })
                .AddMarketplace(config.GetSection("MarketplaceOptions"));

            NLog.LogManager.Configuration = new NLogLoggingConfiguration(config.GetSection("NLog"));

            switch (options.ERP)
            {
                case "XML1":
                    serviceBuilder.AddXml1Connectors(config.GetSection("SimpleNewFilesMonitoringOptions"));
                    break;
                case "CSV12":
                    serviceBuilder.AddCsv12Connectors(config.GetSection("GentleNewFilesMonitoringOptions"));
                    break;
                case "CSV14":
                    serviceBuilder.AddCsv14Connectors(config.GetSection("GentleNewFilesMonitoringOptions"));
                    break;
                default:
                    throw new ApplicationException($"Invalid ERP: '{options.ERP}' (unknown value)");
            }

            switch (options.OFD)
            {
                case OfdYaRuService.NAME:
                    serviceBuilder.AddOfdYaRu(config.GetSection(OfdYaRuService.NAME));
                    break;
                default:
                    throw new ApplicationException($"Invalid OFD: '{options.OFD}' (unknown value)");
            }

            var services = serviceBuilder.BuildServiceProvider();

            var loggerFactory = services.GetRequiredService<ILoggerFactory>();
            var logger = loggerFactory.CreateLogger(nameof(Program));

            var taskCancellationSource = new CancellationTokenSource();

            Console.TreatControlCAsInput = false;
            Console.CancelKeyPress += (s, e) =>
            {
                logger.LogWarning("Ctrl+C pressed. Shutting down...");
                taskCancellationSource.Cancel();
            };

            var lockFile = new FileInfo(Path.Combine(WorkingDirectory, typeof(Program).Assembly.GetName().Name + ".lock"));
            FileStream lockStream = default;

            try
            {
                lockStream = lockFile.Open(FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.Read);

                var ass = typeof(Program).Assembly;
                logger.LogInformation(ass.FullName);

                if (options.KeepWatching)
                {
                    logger.LogInformation("STARTING (Keep-Watching mode). Press Ctrl+C to break.");

                    ITask task = services.GetRequiredService<ITask<Connectors.INewFilesMonitoringTask>>();
                    task.Options.FirstRunDelay = TimeSpan.FromSeconds(10);
                    task.Start(taskCancellationSource.Token);

                    task = services.GetRequiredService<ITask<Connectors.GetMatchingTask>>();
                    task.Options.FirstRunDelay = TimeSpan.FromSeconds(3);
                    task.Start(taskCancellationSource.Token);

                    await Task.Delay(-1, taskCancellationSource.Token);
                }
                else
                {
                    logger.LogInformation("STARTING (One-Pass mode). Press Ctrl+C to break.");

                    using (var scope = services.CreateScope())
                    {
                        var matchingTask = services.GetRequiredService<Connectors.GetMatchingTask>();
                        await matchingTask.RunAsync(null, scope.ServiceProvider, taskCancellationSource.Token);

                        var monitoringTask = services.GetRequiredService<Connectors.INewFilesMonitoringTask>();
                        await monitoringTask.RunAsync(null, scope.ServiceProvider, taskCancellationSource.Token);
                    }
                }

                logger.LogInformation("FINISHED.");

                // wait a little to flush all logs
                await Task.Delay(1000);
            }
            finally
            {
                lockStream?.Close();
                lockFile.Delete();
            }

            Console.WriteLine("FINISHED.");
        }
    }
}
