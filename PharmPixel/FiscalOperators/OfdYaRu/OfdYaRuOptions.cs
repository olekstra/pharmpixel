﻿namespace PharmPixel.FiscalOperators.OfdYaRu
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class OfdYaRuOptions
    {
        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> { new DecimalJsonConverter() },
        };

        public Uri BaseUrl { get; set; } = new Uri("https://api.ofd-ya.ru");

        public string OfdApiToken { get; set; }
    }
}
