﻿namespace PharmPixel.FiscalOperators.OfdYaRu
{
    using System;
    using System.Threading.Tasks;
    using PharmPixel.FiscalOperators.OfdYaRu.Dto;
    using Refit;

    public interface IOfdYaRuClient
    {
        [Post("/ofdapi/v1/KKT")]
        Task<KktResponse> GetKktAsync([Body]KktRequest request);

        [Post("/ofdapi/v1/documents")]
        Task<DocumentsResponse> GetDocumentsAsync([Body]DocumentsRequest request);

        [Post("/ofdapi/v1/getChequeLink")]
        Task<GetChequeLinkResponse> GetChequeLinkAsync([Body]GetChequeLinkRequest request);
    }
}
