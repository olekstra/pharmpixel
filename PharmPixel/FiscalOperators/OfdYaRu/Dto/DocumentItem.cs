﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    public class DocumentItem
    {
        public decimal Quantity { get; set; }

        public decimal Price { get; set; }

        public string Name { get; set; }

        public decimal Sum { get; set; }

        public int ProductType { get; set; }

        public int PaymentType { get; set; }
    }
}
