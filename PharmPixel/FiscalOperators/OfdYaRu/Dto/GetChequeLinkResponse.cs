﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    public class GetChequeLinkResponse : IErrorResponse
    {
        int? IErrorResponse.Code { get; set; }

        string IErrorResponse.Desc { get; set; }

        public string DocType { get; set; }

        public string Link { get; set; }

        public Document FiscalDocument { get; set; }
    }
}
