﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    public interface IErrorResponse
    {
        int? Code { get; set; }

        string Desc { get; set; }
    }
}
