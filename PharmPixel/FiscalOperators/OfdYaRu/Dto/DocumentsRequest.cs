﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;

    public class DocumentsRequest
    {
        public DocumentsRequest(string fiscalDriveNumber, DateTime date)
        {
            this.FiscalDriveNumber = fiscalDriveNumber;
            this.Date = date;
        }

        public string FiscalDriveNumber { get; set; }

        [JsonIgnore]
        public DateTime Date { get; set; }

        [JsonProperty("date")]
        public string DateAsString
        {
            get
            {
                return Date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            }

            set
            {
                Date = DateTime.ParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
        }

        [JsonIgnore]
        public int Start { get; set; } = 1;

        [JsonIgnore]
        public int End { get; set; } = 3000;

        public string Interval
        {
            get
            {
                return $"{Start},{End}";
            }
        }
    }
}
