﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    using System.Collections.Generic;

    public class DocumentsResponse : IErrorResponse
    {
        int? IErrorResponse.Code { get; set; }

        string IErrorResponse.Desc { get; set; }

        public int Count { get; set; }

        public List<Document> Items { get; set; }
    }
}
