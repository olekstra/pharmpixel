﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    using System;
    using Newtonsoft.Json;

    public class Document
    {
        [JsonConverter(typeof(UnixTimeDateTimeOffsetConverter))]
        public DateTimeOffset DateTime { get; set; }

        public decimal ProvisionSum { get; set; }

        public int FiscalDocumentFormatVer { get; set; }

        public int Code { get; set; }

        public string FiscalDriveNumber { get; set; }

        public int ShiftNumber { get; set; }

        [JsonConverter(typeof(UnixTimeDateTimeOffsetConverter))]
        public DateTimeOffset ReceivingDate { get; set; }

        public string Operator { get; set; }

        public int RequestNumber { get; set; }

        public decimal EcashTotalSum { get; set; }

        public long FiscalDocumentNumber { get; set; }

        public int TaxationType { get; set; }

        public decimal NdsNo { get; set; }

        public decimal Nds0 { get; set; }

        public decimal Nds10 { get; set; }

        public decimal Nds18 { get; set; }

        public decimal Nds18118 { get; set; }

        public decimal Nds10110 { get; set; }

        public string UserInn { get; set; }

        public decimal CreditSum { get; set; }

        public string KktRegId { get; set; }

        public decimal CashTotalSum { get; set; }

        public decimal TotalSum { get; set; }

        public string AuthorityUri { get; set; }

        public string RetailAddress { get; set; }

        public string FiscalSign { get; set; }

        public int OperationType { get; set; }

        public decimal PrepaidSum { get; set; }

        public DocumentItem[] Items { get; set; }

        public string RetailPlace { get; set; }

        public string User { get; set; }
    }
}
