﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    using System;
    using System.Collections.Generic;

    public class KktResponse : IErrorResponse
    {
        int? IErrorResponse.Code { get; set; }

        string IErrorResponse.Desc { get; set; }

        public Dictionary<string, KKtInfo> Kkt { get; set; }

        public int Count { get; set; }

        public class KKtInfo
        {
            public string Address { get; set; }

            public string Last { get; set; }

            public string Kktregid { get; set; }

            public decimal Turnover { get; set; }

            public int ReceiptCount { get; set; }
        }
    }
}