﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;

    public class KktRequest
    {
        public KktRequest(DateTime date)
        {
            this.Date = date;
        }

        [JsonIgnore]
        public DateTime Date { get; set; }

        [JsonProperty("date")]
        public string DateAsString
        {
            get
            {
                return Date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
            }

            set
            {
                Date = DateTime.ParseExact(value, "yyyy-MM-dd", CultureInfo.InvariantCulture);
            }
        }
    }
}
