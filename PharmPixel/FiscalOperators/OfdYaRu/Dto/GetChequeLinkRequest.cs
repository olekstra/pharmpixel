﻿namespace PharmPixel.FiscalOperators.OfdYaRu.Dto
{
    public class GetChequeLinkRequest
    {
        public GetChequeLinkRequest(string fiscalDriveNumber, long fiscalDocumentNumber)
        {
            this.FiscalDriveNumber = fiscalDriveNumber;
            this.FiscalDocumentNumber = fiscalDocumentNumber;
        }

        public string FiscalDriveNumber { get; set; }

        public long FiscalDocumentNumber { get; set; }
    }
}
