﻿namespace PharmPixel.FiscalOperators.OfdYaRu
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using PharmPixel.FiscalOperators.OfdYaRu.Dto;

    public class OfdYaRuService : IFiscalOperatorService
    {
        public const string NAME = "OFD_YA";

        private readonly ILogger logger;

        private readonly IOfdYaRuClient client;

        public OfdYaRuService(ILogger<OfdYaRuService> logger, IOfdYaRuClient client)
        {
            this.logger = logger;
            this.client = client;
        }

        public async Task<List<FiscalDocument>> GetFiscalDocumentsAsync(DateTime date)
        {
            const int batchSize = 2500;

            var kkmResponse = await client.GetKktAsync(new KktRequest(date));
            EnsureSuccessfulResponse(kkmResponse);

            var allKkms = kkmResponse.Kkt.Keys.ToArray();
            var activeKkms = kkmResponse.Kkt.Where(x => x.Value.ReceiptCount > 0).Select(x => x.Key).ToArray();

            logger.LogInformation("All KKMs: " + string.Join(',', allKkms));
            logger.LogInformation("Active KKMs: " + string.Join(',', activeKkms));

            var result = new List<FiscalDocument>();

            foreach (var kkm in activeKkms)
            {
                var start = 1;
                var documentsCount = 0;
                var previousItemsCount = result.Count;

                while (true)
                {
                    var docRequest = new DocumentsRequest(kkm, date) { Start = start, End = start + batchSize - 1 };

                    var docResponse = await client.GetDocumentsAsync(docRequest);
                    EnsureSuccessfulResponse(docResponse);

                    if (docResponse.Items.Count == 0)
                    {
                        break;
                    }

                    documentsCount += docResponse.Items.Count;
                    result.AddRange(docResponse.Items.SelectMany(x => Convert(x)));

                    logger.LogDebug($"KKT {kkm}: {documentsCount} documents / {result.Count - previousItemsCount} items loaded so far...");

                    start += batchSize;
                }

                logger.LogInformation($"KKT {kkm}: {documentsCount} documents / {result.Count - previousItemsCount} items loaded.");
            }

            return result;
        }

        public async Task<string> GetLinkAsync(FiscalDocument document)
        {
            var req = new GetChequeLinkRequest(document.FiscalDriveNumber, document.FiscalDocumentNumber);

            var resp = await client.GetChequeLinkAsync(req);

            EnsureSuccessfulResponse(resp);

            return resp.Link;
        }

        public IEnumerable<FiscalDocument> Convert(Document ofdDocument)
        {
            return ofdDocument.Items.Select(x => new FiscalDocument
            {
                CashTotalSum = ofdDocument.CashTotalSum,
                CreatedAt = ofdDocument.ReceivingDate,
                DateTime = ofdDocument.DateTime,
                ECashTotalSum = ofdDocument.EcashTotalSum,
                FiscalDocumentNumber = ofdDocument.FiscalDocumentNumber,
                FiscalDriveNumber = ofdDocument.FiscalDriveNumber,
                Inn = ofdDocument.UserInn,
                KktRegId = ofdDocument.KktRegId,
                Name = x.Name,
                NdsTotalSum = ofdDocument.Nds0 + ofdDocument.Nds10 + ofdDocument.Nds10110 + ofdDocument.Nds18 + ofdDocument.Nds18118 + ofdDocument.NdsNo,
                Ofd = NAME,
                OperationType = ofdDocument.OperationType,
                Price = x.Price,
                Quantity = x.Quantity,
                TaxationType = ofdDocument.TaxationType,
                Total = x.Price * x.Quantity,
                TotalSum = ofdDocument.TotalSum,
                UpdatedAt = DateTimeOffset.Now,
            });
        }

        protected void EnsureSuccessfulResponse(IErrorResponse resp)
        {
            if (resp.Code.HasValue)
            {
                throw new ApplicationException($"Unexpected response: code={resp.Code}, desc={resp.Desc}");
            }
        }
    }
}
