﻿namespace Microsoft.Extensions.DependencyInjection
{
    using System;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using PharmPixel.FiscalOperators;
    using PharmPixel.FiscalOperators.OfdYaRu;
    using Polly;
    using Refit;

    public static class OfdYaRuServiceCollectionExtensions
    {
        public static IServiceCollection AddOfdYaRu(this IServiceCollection services, IConfigurationSection configurationSection)
        {
            var refitSettings = new RefitSettings()
            {
                Buffered = true,
                ContentSerializer = new JsonContentSerializer(OfdYaRuOptions.JsonSerializerSettings),
            };

            services.Configure<OfdYaRuOptions>(configurationSection);

            services.AddScoped<IFiscalOperatorService, OfdYaRuService>();

            services.AddRefitClient<IOfdYaRuClient>(refitSettings)
                    .ConfigureHttpClient((sp, c) =>
                    {
                        var options = sp.GetRequiredService<IOptionsSnapshot<OfdYaRuOptions>>().Value;
                        c.BaseAddress = options.BaseUrl;
                        c.DefaultRequestHeaders.Add("Ofdapitoken", options.OfdApiToken);
                    })
                    .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
                    {
                        TimeSpan.FromSeconds(1),
                        TimeSpan.FromSeconds(5),
                        TimeSpan.FromSeconds(10),
                    }));

            return services;
        }
    }
}
