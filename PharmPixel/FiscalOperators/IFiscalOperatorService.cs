﻿namespace PharmPixel.FiscalOperators
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IFiscalOperatorService
    {
        Task<List<FiscalDocument>> GetFiscalDocumentsAsync(DateTime date);

        Task<string> GetLinkAsync(FiscalDocument document);
    }
}
