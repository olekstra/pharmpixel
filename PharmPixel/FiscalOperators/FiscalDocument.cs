﻿namespace PharmPixel.FiscalOperators
{
    using System;

    public class FiscalDocument
    {
        public decimal CashTotalSum { get; set; }

        public DateTimeOffset CreatedAt { get; set; }

        public DateTimeOffset DateTime { get; set; }

        public decimal ECashTotalSum { get; set; }

        public long FiscalDocumentNumber { get; set; }

        public string FiscalDriveNumber { get; set; }

        public string Inn { get; set; }

        public string KktRegId { get; set; }

        public string Name { get; set; }

        public decimal NdsTotalSum { get; set; }

        public string Ofd { get; set; }

        public int OperationType { get; set; }

        public decimal Price { get; set; }

        public decimal Quantity { get; set; }

        public int TaxationType { get; set; }

        public decimal Total { get; set; }

        public decimal TotalSum { get; set; }

        public DateTimeOffset UpdatedAt { get; set; }
    }
}
