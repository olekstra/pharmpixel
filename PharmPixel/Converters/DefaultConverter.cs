﻿namespace PharmPixel.Converters
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using PharmPixel.FiscalOperators;
    using PharmPixel.Marketplace;
    using PharmPixel.Marketplace.Dto;

    public static class DefaultConverter
    {
        private static readonly StringComparer DefaultComparer = StringComparer.InvariantCultureIgnoreCase;

        /// <summary>
        /// Максимально допустимая "разница" между временем продажи в ОФД и ERP.
        /// </summary>
        public static TimeSpan MaxAllowedBias { get; set; } = TimeSpan.FromMinutes(1);

        /// <summary>
        /// Выполняет подготовку данных о товарах для отправки:
        /// 1. Сворачивает по повторяющимся manufacturer+name.
        /// </summary>
        /// <param name="data">Входные данные.</param>
        /// <returns>Данные для выгрузки.</returns>
        public static List<SendProductsRequest.Item>
            PrepareProducts(IEnumerable<(string manufacturer, string name)> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            return data
                .Distinct(new TwoStringsComparer())
                .Select(x => new SendProductsRequest.Item { Manufacturer = x.Item1, Name = x.Item2 })
                .ToList();
        }

        /// <summary>
        /// Выполняет подготовку данных об остатках для отправки:
        /// 1. Фильтрует по manufacturer+name исходя из меппинга от marketplace
        /// 2. Сворачивает по повторяющимся manufacturer+name+provider.
        /// </summary>
        /// <param name="data">Входные данные по остаткам.</param>
        /// <param name="marketplaceService">Экземпляр сервиса для получения меппинга.</param>
        /// <returns>Данные по остаткам для выгрузки.</returns>
        public static async Task<List<SendStockRequest.Item>>
            PrepareStockAsync(IEnumerable<(string manufacturer, string name, string provider, decimal stock)> data, IMarketplaceService marketplaceService)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (marketplaceService is null)
            {
                throw new ArgumentNullException(nameof(marketplaceService));
            }

            var matchList = await marketplaceService.GetExportableMappingsAsync();
            var matches = matchList
                .GroupBy(x => x.manufacturer, DefaultComparer)
                .ToDictionary(
                    x => x.Key,
                    x => x.Select(y => y.name).Distinct(DefaultComparer).ToHashSet(DefaultComparer),
                    DefaultComparer);

            if (matches.Count == 0)
            {
                return new List<SendStockRequest.Item>();
            }

            return data
                .Where(x => matches.TryGetValue(x.manufacturer, out var xx) && xx.Contains(x.name))
                .GroupBy(x => (x.manufacturer, x.name, x.provider), x => x.stock, new ThreeStringsComparer())
                .Select(x => new SendStockRequest.Item
                {
                    Manufacturer = x.Key.Item1,
                    Name = x.Key.Item2,
                    Provider = x.Key.Item3,
                    Stock = x.Sum(y => y),
                })
                .ToList();
        }

        /// <summary>
        /// Выполняет подготовку данных об продажах для отправки:
        /// 1. Фильтрует по manufacturer+name исходя из меппинга от marketplace
        /// 2. Стыкует с данными из ОФД
        /// 3. Формирует списки valid и invalid для выгрузки в маркетплейс.
        /// </summary>
        /// <param name="data">Входные данные по остаткам.</param>
        /// <param name="marketplaceService">Экземпляр сервиса для получения меппинга.</param>
        /// <param name="fiscalOperatorService">Экземпляр сервиса для взаимодействия с ОФД.</param>
        /// <returns>Данные по продажам для выгрузки.</returns>
        public static async Task<(List<SendSalesValidRequest.Item> valid, List<SendSalesInvalidRequest.Item> invalid)>
            PrepareSalesAsync(IEnumerable<SaleData> data, IMarketplaceService marketplaceService, IFiscalOperatorService fiscalOperatorService, bool usePartialMatch)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (marketplaceService is null)
            {
                throw new ArgumentNullException(nameof(marketplaceService));
            }

            var matchList = await marketplaceService.GetExportableMappingsAsync();
            var matches = matchList
                .GroupBy(x => x.manufacturer, DefaultComparer)
                .ToDictionary(
                    x => x.Key,
                    x => x.GroupBy(y => y.name, DefaultComparer).ToDictionary(y => y.Key, y => y.First().id, DefaultComparer),
                    DefaultComparer);

            var valid = new List<SendSalesValidRequest.Item>();
            var invalid = new List<SendSalesInvalidRequest.Item>();

            if (matches.Count == 0)
            {
                return (valid, invalid);
            }

            var salesToValidate = data
                .Select(x =>
                    matches.TryGetValue(x.Manufacturer, out var xx) && xx.TryGetValue(x.Name, out var xxx)
                    ? (sale: x, id: xxx)
                    : (sale: x, id: null))
                .Where(x => x.id != null)
                .GroupBy(x => x.sale.DateTime.Date);

            foreach (var day in salesToValidate)
            {
                var daySales = await fiscalOperatorService.GetFiscalDocumentsAsync(day.Key);
                var (newValid, newInvalid) = await CompareSales(day.ToList(), daySales, x => fiscalOperatorService.GetLinkAsync(x), usePartialMatch);
                valid.AddRange(newValid);
                invalid.AddRange(newInvalid);
            }

            return (valid, invalid);
        }

        private static async Task<(List<SendSalesValidRequest.Item> valid, List<SendSalesInvalidRequest.Item> invalid)>
            CompareSales(IList<(SaleData sale, string id)> candidates, List<FiscalDocument> fiscalDocuments, Func<FiscalDocument, Task<string>> linkGenerator, bool usePartialMatch)
        {
            var matches = candidates
                .Join(
                    fiscalDocuments,
                    x => (x.sale.Name, x.sale.Quantity, x.sale.DateTime, x.sale.Number),
                    y => (y.Name, y.Quantity, y.DateTime.DateTime, y.FiscalDocumentNumber.ToString()),
                    (x, y) => (x, y),
                    new SaleDataComparer() { UsePartialMatch = usePartialMatch })
                .ToDictionary(x => x.x, x => x.y);

            var valid = new List<SendSalesValidRequest.Item>();
            foreach (var x in matches)
            {
                var link = await linkGenerator(x.Value);
                valid.Add(new SendSalesValidRequest.Item
                {
                    CashTotalSum = x.Value.CashTotalSum,
                    CreatedAt = x.Value.CreatedAt,
                    DateTime = x.Value.DateTime,
                    ECashTotalSum = x.Value.ECashTotalSum,
                    FiscalDocumentNumber = x.Value.FiscalDocumentNumber,
                    FiscalDriveNumber = x.Value.FiscalDriveNumber,
                    Inn = x.Value.Inn,
                    KktRegId = x.Value.KktRegId,
                    Link = link,
                    Name = x.Value.Name,
                    NdsTotalSum = x.Value.NdsTotalSum,
                    Ofd = x.Value.Ofd,
                    OperationType = x.Value.OperationType,
                    Price = x.Value.Price,
                    PriceSellIn = x.Key.sale.PriceSellIn,
                    ProductId = x.Key.id,
                    Quantity = x.Value.Quantity,
                    TaxationType = x.Value.TaxationType,
                    Total = x.Value.Total,
                    TotalSum = x.Value.TotalSum,
                    UpdatedAt = x.Value.UpdatedAt,
                });
            }

            var invalid = candidates
                .Where(x => !matches.ContainsKey(x))
                .Select(x => new SendSalesInvalidRequest.Item
                {
                    Name = x.sale.Name,
                    ProductId = x.id,
                    Manufacturer = x.sale.Manufacturer,
                    Provider = x.sale.Provider,
                    PriceSellOut = x.sale.PriceSellOut,
                    PriceSellIn = x.sale.PriceSellIn,
                    Quantity = x.sale.Quantity,
                    DatePay = x.sale.DateTime,
                    Number = x.sale.Number,
                })
                .ToList();

            return (valid, invalid);
        }

        public class TwoStringsComparer : IEqualityComparer<(string, string)>
        {
            public bool Equals((string, string) x, (string, string) y)
            {
                return DefaultComparer.Compare(x.Item1, y.Item1) == 0
                    && DefaultComparer.Compare(x.Item2, y.Item2) == 0;
            }

            public int GetHashCode((string, string) obj)
            {
                return obj.Item1?.Length ?? 0 + obj.Item2?.Length ?? 0;
            }
        }

        public class ThreeStringsComparer : IEqualityComparer<(string, string, string)>
        {
            public bool Equals((string, string, string) x, (string, string, string) y)
            {
                return DefaultComparer.Compare(x.Item1, y.Item1) == 0
                    && DefaultComparer.Compare(x.Item2, y.Item2) == 0
                    && DefaultComparer.Compare(x.Item3, y.Item3) == 0;
            }

            public int GetHashCode((string, string, string) obj)
            {
                return obj.Item1?.Length ?? 0 + obj.Item2?.Length ?? 0 + obj.Item3?.Length ?? 0;
            }
        }

        /// <summary>
        /// Сверка продаж по алгоритму:
        /// * сверять по названию(по startsWith, в ОФД может быть обрезано) и quantity
        /// * если заполнен number - то сравнивать с fiscalDocumentNumber
        /// * если не заполнен - по datePay с dateTime плюс-минус "в настройках".
        /// </summary>
        public class SaleDataComparer : IEqualityComparer<(string, decimal, DateTime, string)>
        {
            public bool UsePartialMatch { get; set; } = false;

            public bool Equals((string, decimal, DateTime, string) x, (string, decimal, DateTime, string) y)
            {
                // количество должно совпадать
                if (x.Item2 != y.Item2)
                {
                    return false;
                }

                // одно из названий может быть обрезанным, сверяем минимальную длину
                var min = Math.Min(x.Item1.Length, y.Item1.Length);
                if (!string.Equals(x.Item1.Substring(0, min), y.Item1.Substring(0, min), StringComparison.OrdinalIgnoreCase))
                {
                    return false;
                }

                if (UsePartialMatch)
                {
                    return true;
                }

                // если нет номеров - по дате-времени
                if (string.IsNullOrEmpty(x.Item4) || string.IsNullOrEmpty(y.Item4))
                {
                    if ((x.Item3 - y.Item3).Duration() > MaxAllowedBias)
                    {
                        return false;
                    }
                }
                else
                {
                    if (!string.Equals(x.Item4, y.Item4, StringComparison.Ordinal))
                    {
                        return false;
                    }
                }

                return true;
            }

            public int GetHashCode((string, decimal, DateTime, string) obj)
            {
                // только количество должно быть "точно равно",
                // остальные пля "равны приблизительно" и поэтому не могут напрямую участвовать в генерации хэш-кода
                return obj.Item2.GetHashCode();
            }
        }

        public class SaleData
        {
            public string Name { get; set; }

            public string Manufacturer { get; set; }

            public string Provider { get; set; }

            public decimal PriceSellOut { get; set; }

            public decimal PriceSellIn { get; set; }

            public decimal Quantity { get; set; }

            public DateTime DateTime { get; set; }

            public string Number { get; set; }
        }
    }
}
