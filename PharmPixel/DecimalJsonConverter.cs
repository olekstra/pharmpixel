﻿namespace PharmPixel
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;

    public class DecimalJsonConverter : JsonConverter<decimal>
    {
        public override void WriteJson(JsonWriter writer, decimal value, JsonSerializer serializer)
        {
            writer.WriteRawValue(value.ToString(CultureInfo.InvariantCulture));
        }

        public override decimal ReadJson(JsonReader reader, Type objectType, decimal existingValue, bool hasExistingValue, JsonSerializer serializer)
        {
            string s = reader.TokenType == JsonToken.String
                ? (string)reader.Value
                : reader.Value.ToString();
            return decimal.Parse(s, CultureInfo.InvariantCulture);
        }
    }
}
