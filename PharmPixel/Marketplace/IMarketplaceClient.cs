﻿namespace PharmPixel.Marketplace
{
    using System;
    using System.Threading.Tasks;
    using PharmPixel.Marketplace.Dto;
    using Refit;

    public interface IMarketplaceClient
    {
        [Post("/api/v1/users/login")]
        Task<LoginResponse> LoginAsync([Body]LoginRequest request);

        [Post("/api/v1/erp/products")]
        Task<SendProductsResponse> SendProductsAsync([Header("Authorization")] string token, [Body]SendProductsRequest request);

        [Get("/api/v1/erp/products/match")]
        Task<GetMatchesResponse> GetMatchesAsync([Header("Authorization")] string token);

        [Post("/api/v1/erp/products/stock")]
        Task<SendStockResponse> SendStockAsync([Header("Authorization")] string token, [Body]SendStockRequest request);

        [Post("/api/v1/erp/sales/valid")]
        Task<SendSalesResponse> SendSalesValidAsync([Header("Authorization")] string token, [Body]SendSalesValidRequest request);

        [Post("/api/v1/erp/sales")]
        Task<SendSalesResponse> SendSalesInvalidAsync([Header("Authorization")] string token, [Body]SendSalesInvalidRequest request);
    }
}
