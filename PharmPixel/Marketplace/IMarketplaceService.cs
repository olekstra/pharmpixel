﻿namespace PharmPixel.Marketplace
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using PharmPixel.Marketplace.Dto;

    public interface IMarketplaceService
    {
        Task LoginAsync();

        Task SendProductsAsync(List<SendProductsRequest.Item> data);

        Task SendStockAsync(List<SendStockRequest.Item> data);

        Task SendSalesValidAsync(List<SendSalesValidRequest.Item> data);

        Task SendSalesInvalidAsync(List<SendSalesInvalidRequest.Item> data);

        Task GetAndSaveProductMatchesAsync();

        Task<IList<(string manufacturer, string name, string id)>> GetExportableMappingsAsync();
    }
}
