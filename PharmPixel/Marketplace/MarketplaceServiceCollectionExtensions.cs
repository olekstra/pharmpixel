﻿namespace Microsoft.Extensions.DependencyInjection
{
    using System;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.Options;
    using PharmPixel.Marketplace;
    using Polly;
    using Refit;

    public static class MarketplaceServiceCollectionExtensions
    {
        public static IServiceCollection AddMarketplace(this IServiceCollection services, IConfigurationSection configurationSection)
        {
            var refitSettings = new RefitSettings()
            {
                Buffered = true,
                ContentSerializer = new JsonContentSerializer(MarketplaceOptions.JsonSerializerSettings),
            };

            services.Configure<MarketplaceOptions>(configurationSection);

            services.AddScoped<IMarketplaceService, MarketplaceService>();

            services.AddRefitClient<IMarketplaceClient>(refitSettings)
                    .ConfigureHttpClient((sp, c) => c.BaseAddress = sp.GetRequiredService<IOptions<MarketplaceOptions>>().Value.BaseUrl)
                    .AddTransientHttpErrorPolicy(builder => builder.WaitAndRetryAsync(new[]
                    {
                        TimeSpan.FromSeconds(1),
                        TimeSpan.FromSeconds(5),
                        TimeSpan.FromSeconds(10),
                    }));

            return services;
        }
    }
}
