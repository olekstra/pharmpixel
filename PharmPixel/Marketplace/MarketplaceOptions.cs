﻿namespace PharmPixel.Marketplace
{
    using System;
    using System.Collections.Generic;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class MarketplaceOptions
    {
        public static readonly JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new CamelCasePropertyNamesContractResolver(),
            Converters = new List<JsonConverter> { new DecimalJsonConverter(), new IsoUtcDateTimeOffsetConverter() },
            DateParseHandling = DateParseHandling.None,
        };

        public Uri BaseUrl { get; set; }

        public string Username { get; set; }

        public string Password { get; set; }
    }
}
