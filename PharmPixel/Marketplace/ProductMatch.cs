﻿namespace PharmPixel.Marketplace
{
    public class ProductMatch
    {
        public bool Export { get; set; }

        public string Id { get; set; }

        public string Manufacturer { get; set; }

        public string Name { get; set; }
    }
}
