﻿namespace PharmPixel.Marketplace
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using Newtonsoft.Json;
    using PharmPixel.Marketplace.Dto;

    public class MarketplaceService : IMarketplaceService
    {
        private const string MatchesFileName = "matches.json";

        private readonly ILogger logger;

        private readonly IMarketplaceClient marketplaceClient;

        private readonly MarketplaceOptions options;

        private List<ProductMatch> productMatches;

        public MarketplaceService(ILogger<MarketplaceService> logger, IMarketplaceClient marketplaceClient, IOptionsSnapshot<MarketplaceOptions> options)
        {
            this.logger = logger;
            this.marketplaceClient = marketplaceClient;
            this.options = options.Value;
        }

        public string AuthorizationToken { get; set; } = null;

        public async Task LoginAsync()
        {
            var req = new LoginRequest
            {
                Email = options.Username,
                Password = options.Password,
            };

            var resp = await marketplaceClient.LoginAsync(req);

            EnsureSuccessfulResponse(resp);

            AuthorizationToken = resp.Data.AccessToken;

            logger.LogInformation($"Login: successful for '{req.Email}'");
        }

        public async Task SendProductsAsync(List<SendProductsRequest.Item> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (AuthorizationToken == null)
            {
                await LoginAsync();
            }

            var req = new SendProductsRequest() { Data = data };

            var resp = await marketplaceClient.SendProductsAsync(AuthorizationToken, req);

            EnsureSuccessfulResponse(resp);

            logger.LogInformation($"SendProducts: {data.Count} items sent.");
        }

        public async Task SendStockAsync(List<SendStockRequest.Item> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (AuthorizationToken == null)
            {
                await LoginAsync();
            }

            var req = new SendStockRequest { Data = data };

            var resp = await marketplaceClient.SendStockAsync(AuthorizationToken, req);

            EnsureSuccessfulResponse(resp);

            logger.LogInformation($"SendStock: {data.Count} items sent.");
        }

        public async Task SendSalesValidAsync(List<SendSalesValidRequest.Item> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (AuthorizationToken == null)
            {
                await LoginAsync();
            }

            var req = new SendSalesValidRequest { Data = data };

            var resp = await marketplaceClient.SendSalesValidAsync(AuthorizationToken, req);

            EnsureSuccessfulResponse(resp);

            logger.LogInformation($"SendSalesValid: {data.Count} items sent.");
        }

        public async Task SendSalesInvalidAsync(List<SendSalesInvalidRequest.Item> data)
        {
            if (data == null)
            {
                throw new ArgumentNullException(nameof(data));
            }

            if (AuthorizationToken == null)
            {
                await LoginAsync();
            }

            var req = new SendSalesInvalidRequest { Data = data };

            var resp = await marketplaceClient.SendSalesInvalidAsync(AuthorizationToken, req);

            EnsureSuccessfulResponse(resp);

            logger.LogInformation($"SendSalesInvalid: {data.Count} items sent.");
        }

        public async Task GetAndSaveProductMatchesAsync()
        {
            if (AuthorizationToken == null)
            {
                await LoginAsync();
            }

            var resp = await marketplaceClient.GetMatchesAsync(AuthorizationToken);

            EnsureSuccessfulResponse(resp);

            productMatches = resp.Data
                .Select(x => new ProductMatch { Export = x.Export, Id = x.Id, Manufacturer = x.Manufacturer, Name = x.Name })
                .ToList();

            var exportable = productMatches.Count(x => x.Export);

            logger.LogInformation($"Matches: loaded {productMatches.Count} items, including {exportable} exportable");

            var filePath = Path.Combine(Program.WorkingDirectory, MatchesFileName);
            await File.WriteAllTextAsync(filePath, JsonConvert.SerializeObject(productMatches));

            logger.LogDebug($"Matches: saved to {filePath}");
        }

        public async Task<IList<(string manufacturer, string name, string id)>> GetExportableMappingsAsync()
        {
            if (productMatches == null)
            {
                await LoadMatchesAsync();
            }

            var retval = productMatches
                .Where(x => x.Export)
                .Select(x => (x.Manufacturer, x.Name, x.Id))
                .ToList();

            logger.LogDebug($"Found {retval.Count} 'exportable' mappings.");

            return retval;
        }

        protected void EnsureSuccessfulResponse(ResponseBase response)
        {
            if (response.Status != 200)
            {
                throw new ApplicationException("Unsuccessful response: status = " + response.Status);
            }
        }

        protected async Task LoadMatchesAsync()
        {
            var filePath = Path.Combine(Program.WorkingDirectory, MatchesFileName);
            if (File.Exists(filePath))
            {
                var text = await File.ReadAllTextAsync(filePath);
                productMatches = JsonConvert.DeserializeObject<List<ProductMatch>>(text);
                logger.LogDebug($"Loaded {productMatches.Count} mappings from '{filePath}'");
            }
            else
            {
                logger.LogWarning($"File not found, no matches: {filePath}");
                productMatches = new List<ProductMatch>();
            }
        }
    }
}
