﻿namespace PharmPixel.Marketplace.Dto
{
    using System.Collections.Generic;

    public class SendStockRequest
    {
        public List<Item> Data { get; set; }

        public class Item
        {
            public string Manufacturer { get; set; }

            public string Name { get; set; }

            public string Provider { get; set; }

            public decimal Stock { get; set; }
        }
    }
}
