﻿namespace PharmPixel.Marketplace.Dto
{
    using System.Collections.Generic;

    public class GetMatchesResponse : ResponseBase
    {
        public List<Item> Data { get; set; }

        public class Item
        {
            public bool Export { get; set; }

            public string Id { get; set; }

            public string Manufacturer { get; set; }

            public string Name { get; set; }
        }
    }
}
