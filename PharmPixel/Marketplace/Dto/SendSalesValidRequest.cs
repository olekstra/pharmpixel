﻿namespace PharmPixel.Marketplace.Dto
{
    using System;
    using System.Collections.Generic;

    public class SendSalesValidRequest
    {
        public List<Item> Data { get; set; }

        public class Item
        {
            public decimal CashTotalSum { get; set; }

            public DateTimeOffset CreatedAt { get; set; }

            public DateTimeOffset DateTime { get; set; }

            public decimal ECashTotalSum { get; set; }

            public long FiscalDocumentNumber { get; set; }

            public string FiscalDriveNumber { get; set; }

            public string Inn { get; set; }

            public string KktRegId { get; set; }

            public string Link { get; set; }

            public string Name { get; set; }

            public decimal NdsTotalSum { get; set; }

            public string Ofd { get; set; }

            public int OperationType { get; set; }

            public decimal Price { get; set; }

            public decimal PriceSellIn { get; set; }

            public string ProductId { get; set; }

            public decimal Quantity { get; set; }

            public int TaxationType { get; set; }

            public decimal Total { get; set; }

            public decimal TotalSum { get; set; }

            public DateTimeOffset UpdatedAt { get; set; }
        }
    }
}
