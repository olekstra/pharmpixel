﻿namespace PharmPixel.Marketplace.Dto
{
    public class SendProductsResponse : ResponseBase
    {
        public SendProductsData Data { get; set; }

        public class SendProductsData
        {
            public string Status { get; set; }
        }
    }
}
