﻿namespace PharmPixel.Marketplace.Dto
{
    public class LoginResponse : ResponseBase
    {
        public LoginData Data { get; set; }

        public class LoginData
        {
            public string AccessToken { get; set; }

            public string RefreshToken { get; set; }

            public string UserId { get; set; }
        }
    }
}
