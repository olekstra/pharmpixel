﻿namespace PharmPixel.Marketplace.Dto
{
    using System;
    using System.Collections.Generic;

    public class SendSalesInvalidRequest
    {
        public List<Item> Data { get; set; }

        public class Item
        {
            public string Name { get; set; }

            public string ProductId { get; set; }

            public string Manufacturer { get; set; }

            public string Provider { get; set; }

            public decimal PriceSellOut { get; set; }

            public decimal PriceSellIn { get; set; }

            public decimal Quantity { get; set; }

            public DateTimeOffset DatePay { get; set; }

            public string Number { get; set; }
        }
    }
}
