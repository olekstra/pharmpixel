﻿namespace PharmPixel.Marketplace.Dto
{
    using System.Collections.Generic;

    public class SendProductsRequest
    {
        public List<Item> Data { get; set; }

        public class Item
        {
            public string Manufacturer { get; set; }

            public string Name { get; set; }
        }
    }
}
