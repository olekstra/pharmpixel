﻿namespace PharmPixel.Marketplace.Dto
{
    public class SendSalesResponse : ResponseBase
    {
        public SendSalesData Data { get; set; }

        public class SendSalesData
        {
            public string Status { get; set; }
        }
    }
}
