﻿namespace PharmPixel.Marketplace.Dto
{
    public class SendStockResponse : ResponseBase
    {
        public SendStockData Data { get; set; }

        public class SendStockData
        {
            public string Status { get; set; }
        }
    }
}
