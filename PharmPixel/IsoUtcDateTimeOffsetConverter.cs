﻿namespace PharmPixel
{
    using System;
    using System.Globalization;
    using Newtonsoft.Json;

    public class IsoUtcDateTimeOffsetConverter : JsonConverter
    {
        public override bool CanConvert(Type objectType)
        {
            return objectType == typeof(DateTimeOffset);
        }

        public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
        {
            var s = (string)reader.Value;
            return DateTimeOffset.Parse(s, CultureInfo.InvariantCulture).ToUniversalTime();
        }

        public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
        {
            var dt = (DateTimeOffset)value;
            var text = dt.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
            writer.WriteValue(text);
        }
    }
}
