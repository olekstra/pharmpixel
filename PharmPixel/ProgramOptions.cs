﻿namespace PharmPixel
{
    using System;

    public class ProgramOptions
    {
        public TimeSpan MatchUpdateInterval { get; set; } = TimeSpan.FromHours(1);

        public string ERP { get; set; }

        public string OFD { get; set; }

        public bool KeepWatching { get; set; }
    }
}
