﻿namespace PharmPixel.Connectors
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using PharmPixel.Marketplace;
    using RecurrentTasks;

    public class GentleNewFilesMonitoringTask : IRunnable, INewFilesMonitoringTask
    {
        private readonly ILogger logger;

        private readonly GentleNewFilesMonitoringOptions options;

        public GentleNewFilesMonitoringTask(ILogger<GentleNewFilesMonitoringTask> logger, IOptionsSnapshot<GentleNewFilesMonitoringOptions> options)
        {
            this.logger = logger;
            this.options = options.Value;
        }

        public async Task RunAsync(ITask currentTask, IServiceProvider scopeServiceProvider, CancellationToken cancellationToken)
        {
            if (currentTask != null)
            {
                // options may change
                currentTask.Options.Interval = options.MonitoringInterval;
            }

            var (source, working, done) = GetDirectories(options);
            if (source == null)
            {
                return;
            }

            var markFileAsProcessed = new Action<FileInfo, FileProcessResult>((file, result) => MoveProcessedFile(file, result, done));

            await FindNewFilesAsync(source, working);

            await ProcessFilesAsync(working, markFileAsProcessed, scopeServiceProvider);
        }

        public (DirectoryInfo source, DirectoryInfo working, DirectoryInfo done) GetDirectories(GentleNewFilesMonitoringOptions options)
        {
            var source = new DirectoryInfo(options.SourceFolder);
            if (!source.Exists)
            {
                logger.LogError("Directory does not exist: " + source.FullName);
                return (null, null, null);
            }

            var working = new DirectoryInfo(options.WorkingFolder);
            if (!working.Exists)
            {
                logger.LogError("Directory does not exist: " + working.FullName);
                return (null, null, null);
            }

            var done = working.CreateSubdirectory(options.DoneSubfolder);

            return (source, working, done);
        }

        public void MoveProcessedFile(FileInfo file, FileProcessResult result, DirectoryInfo destination)
        {
            var fileName = file.Name;
            var fileWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            var fileExtension = Path.GetExtension(fileName);

            var newFileName = fileWithoutExtension + "_" + result.ToString().ToUpperInvariant() + fileExtension;

            var destinationFile = Path.Combine(destination.FullName, newFileName);

            // удаляем старый файл, чтобы не упасть при Move
            File.Delete(destinationFile);

            file.MoveTo(destinationFile);
            logger.LogDebug($"{fileName} is {result}, moved to {destination}");
        }

        public Task FindNewFilesAsync(DirectoryInfo source, DirectoryInfo working)
        {
            List<FileInfo> files;

            if (options.DaysToCheck <= 0)
            {
                files = source.EnumerateFiles("*.zip").ToList();
            }
            else
            {
                var today = DateTime.Now.Date;
                var dates = Enumerable
                    .Range(0, options.DaysToCheck)
                    .Select(x => today.AddDays(-x).ToString("ddMMyyyy"))
                    .ToHashSet(StringComparer.OrdinalIgnoreCase);
                files = source.EnumerateFiles("*.zip").Where(x => dates.Contains(x.Name.Substring(0, 8))).ToList();
            }

            logger.LogDebug($"{files.Count} 'actual' files found in {source.FullName}");

            foreach (var file in files)
            {
                var workingCopy = Path.Combine(working.FullName, file.Name);
                var markerFile = workingCopy + ".done";

                if (File.Exists(markerFile))
                {
                    logger.LogInformation("Old file (skipped): " + file.Name);
                }
                else
                {
                    file.CopyTo(workingCopy);
                    File.Open(markerFile, FileMode.OpenOrCreate, FileAccess.ReadWrite).Close();
                    logger.LogInformation("New file (copied for processing): " + file.Name);
                }
            }

            return Task.CompletedTask;
        }

        public async Task ProcessFilesAsync(DirectoryInfo directory, Action<FileInfo, FileProcessResult> markAsProcessed, IServiceProvider serviceProvider)
        {
            var filesCount = 0;

            var marketplaceService = serviceProvider.GetRequiredService<IMarketplaceService>();

            foreach (var handler in options.Handlers)
            {
                var files = directory.GetFiles(handler.pattern).OrderBy(x => x.CreationTime).ToList();
                filesCount += files.Count;

                if (files.Count > 0)
                {
                    logger.LogDebug($"Found {files.Count} files for pattern '{handler.pattern}'...");

                    var processor = (IFileProcessor)serviceProvider.GetRequiredService(handler.handler);

                    if (processor is ILastFileProcessor multiProcessor)
                    {
                        // пропускаем все файлы кроме последнего
                        foreach (var file in files.SkipLast(1))
                        {
                            markAsProcessed(file, FileProcessResult.Skipped);
                        }

                        // и удаляем их из списка
                        files.RemoveRange(0, files.Count - 1);
                    }

                    foreach (var file in files)
                    {
                        try
                        {
                            await processor.ProcessFileAsync(file, marketplaceService);
                            markAsProcessed(file, FileProcessResult.OK);
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, $"Failed to process file {file.Name}");
                            markAsProcessed(file, FileProcessResult.Failed);
                        }
                    }
                }
            }

            logger.Log(
                filesCount == 0 ? LogLevel.Debug : LogLevel.Information,
                $"{filesCount} files processed in {directory.FullName}");
        }
    }
}
