﻿namespace PharmPixel.Connectors
{
    using System;
    using System.Collections.Generic;

    public class GentleNewFilesMonitoringOptions
    {
        public List<(string pattern, Type handler)> Handlers { get; set; }

        public string SourceFolder { get; set; }

        public string WorkingFolder { get; set; }

        public TimeSpan MonitoringInterval { get; set; } = TimeSpan.FromMinutes(1);

        public string DoneSubfolder { get; set; } = "done";

        public int DaysToCheck { get; set; } = 7;
    }
}
