﻿namespace PharmPixel.Connectors
{
    using System;
    using System.Collections.Generic;

    public class SimpleNewFilesMonitoringOptions
    {
        public List<(string pattern, Type handler)> Handlers { get; set; }

        public string Folder { get; set; }

        public TimeSpan MonitoringInterval { get; set; } = TimeSpan.FromMinutes(1);

        public string DoneSubfolder { get; set; } = "done";
    }
}
