﻿namespace PharmPixel.Connectors.Xml1
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using PharmPixel.Connectors.Xml1.Dto;
    using PharmPixel.Converters;
    using PharmPixel.Marketplace;

    public class StockProcessor : IFileProcessor
    {
        private readonly ILogger logger;

        public StockProcessor(ILogger<StockProcessor> logger)
        {
            this.logger = logger;
        }

        public async Task ProcessFileAsync(FileInfo file, IMarketplaceService marketplaceService)
        {
            var stock = XmlUtils.Deserialize<Stock>(file);

            logger.LogDebug($"Loaded {stock.Product.Length} items from {file.Name}");

            var data = await DefaultConverter.PrepareStockAsync(
                stock.Product.Select(x => (x.Manufacturer, x.Name, x.Provider, x.Stock)),
                marketplaceService);

            logger.LogDebug($"Converted to {data.Count} 'prepared' items");

            if (data.Count > 0)
            {
                await marketplaceService.SendStockAsync(data);
            }
        }
    }
}
