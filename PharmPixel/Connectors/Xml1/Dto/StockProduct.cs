﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    [System.Serializable]
    [System.Diagnostics.DebuggerStepThrough]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public class StockProduct
    {
        [System.Xml.Serialization.XmlAttribute("name")]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlAttribute("manufacturer")]
        public string Manufacturer { get; set; }

        [System.Xml.Serialization.XmlAttribute("stock")]
        public decimal Stock { get; set; }

        [System.Xml.Serialization.XmlAttribute("provider")]
        public string Provider { get; set; }
    }
}
