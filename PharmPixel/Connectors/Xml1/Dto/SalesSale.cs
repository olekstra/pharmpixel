﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    using System;
    using System.Globalization;

    [System.Serializable]
    [System.Diagnostics.DebuggerStepThrough]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public class SalesSale
    {
        [System.Xml.Serialization.XmlAttribute("name")]
        public string Name { get; set; }

        [System.Xml.Serialization.XmlAttribute("manufacturer")]
        public string Manufacturer { get; set; }

        [System.Xml.Serialization.XmlAttribute("provider")]
        public string Provider { get; set; }

        [System.Xml.Serialization.XmlAttribute("priceSellOut")]
        public decimal PriceSellOut { get; set; }

        [System.Xml.Serialization.XmlAttribute("priceSellIn")]
        public decimal PriceSellIn { get; set; }

        [System.Xml.Serialization.XmlAttribute("quantity")]
        public decimal Quantity { get; set; }

        [System.Xml.Serialization.XmlAttribute("datePay")]
        public string DatePayString
        {
            get
            {
                return DatePay.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
            }

            set
            {
                DatePay = DateTime.ParseExact(value, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None);
            }
        }

        [System.Xml.Serialization.XmlIgnore]
        public DateTime DatePay { get; set; }

        [System.Xml.Serialization.XmlAttribute("number")]
        public string Number { get; set; }
    }
}
