﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    [System.Serializable]
    [System.Diagnostics.DebuggerStepThrough]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot(ElementName = "stock", Namespace = "", IsNullable = false)]
    public class Stock
    {
        [System.Xml.Serialization.XmlElement("product")]
        public StockProduct[] Product { get; set; }
    }
}
