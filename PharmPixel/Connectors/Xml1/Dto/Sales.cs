﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    [System.Serializable]
    [System.Diagnostics.DebuggerStepThrough]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot(ElementName = "sales", Namespace = "", IsNullable = false)]
    public class Sales
    {
        [System.Xml.Serialization.XmlElement("sale")]
        public SalesSale[] Sale { get; set; }
    }
}
