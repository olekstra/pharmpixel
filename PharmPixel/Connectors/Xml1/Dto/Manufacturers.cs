﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    [System.Serializable]
    [System.Diagnostics.DebuggerStepThrough]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot(ElementName = "manufacturers", Namespace = "", IsNullable = false)]
    public class Manufacturers
    {
        [System.Xml.Serialization.XmlElement("manufacturer")]
        public ManufacturersManufacturer[] Manufacturer { get; set; }
    }
}
