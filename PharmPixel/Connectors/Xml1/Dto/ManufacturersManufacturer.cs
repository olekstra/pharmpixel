﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    [System.Serializable]
    [System.Diagnostics.DebuggerStepThrough]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    public class ManufacturersManufacturer
    {
        [System.Xml.Serialization.XmlAttribute("name")]
        public string Name { get; set; }
    }
}
