﻿namespace PharmPixel.Connectors.Xml1.Dto
{
    [System.Serializable]
    [System.Diagnostics.DebuggerStepThrough]
    [System.ComponentModel.DesignerCategory("code")]
    [System.Xml.Serialization.XmlType(AnonymousType = true)]
    [System.Xml.Serialization.XmlRoot(ElementName = "products", Namespace = "", IsNullable = false)]
    public class Products
    {
        [System.Xml.Serialization.XmlElement("product")]
        public ProductsProduct[] Product { get; set; }
    }
}
