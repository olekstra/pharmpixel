﻿namespace PharmPixel.Connectors.Xml1
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using PharmPixel.Connectors.Xml1.Dto;
    using PharmPixel.Converters;
    using PharmPixel.FiscalOperators;
    using PharmPixel.Marketplace;

    public class SalesProcessor : IFileProcessor
    {
        private readonly ILogger logger;

        private readonly IFiscalOperatorService fiscalOperatorService;

        public SalesProcessor(ILogger<SalesProcessor> logger, IFiscalOperatorService fiscalOperatorService)
        {
            this.logger = logger;
            this.fiscalOperatorService = fiscalOperatorService;
        }

        public async Task ProcessFileAsync(FileInfo file, IMarketplaceService marketplaceService)
        {
            var list = XmlUtils.Deserialize<Sales>(file);

            logger.LogDebug($"Loaded {list.Sale.Length} items from {file.Name}");

            var sales = list.Sale.Select(x =>
                new DefaultConverter.SaleData
                {
                    Name = x.Name,
                    Manufacturer = x.Manufacturer,
                    Provider = x.Provider,
                    PriceSellIn = x.PriceSellIn,
                    PriceSellOut = x.PriceSellOut,
                    Quantity = x.Quantity,
                    DateTime = x.DatePay,
                    Number = x.Number,
                });

            var (valid, invalid) = await DefaultConverter.PrepareSalesAsync(sales, marketplaceService, fiscalOperatorService, false);

            logger.LogDebug($"Converted to {valid.Count} 'valid' and {invalid.Count} 'invalid' items");

            if (valid.Count > 0)
            {
                await marketplaceService.SendSalesValidAsync(valid);
            }

            if (invalid.Count > 0)
            {
                await marketplaceService.SendSalesInvalidAsync(invalid);
            }
        }
    }
}
