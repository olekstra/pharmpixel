﻿namespace PharmPixel.Connectors.Xml1
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using PharmPixel.Connectors.Xml1.Dto;
    using PharmPixel.Converters;
    using PharmPixel.Marketplace;

    public class ProductsProcessor : ILastFileProcessor
    {
        private readonly ILogger logger;

        public ProductsProcessor(ILogger<ProductsProcessor> logger)
        {
            this.logger = logger;
        }

        public async Task ProcessFileAsync(FileInfo file, IMarketplaceService marketplaceService)
        {
            var products = XmlUtils.Deserialize<Products>(file);

            logger.LogDebug($"Loaded {products.Product.Length} items from {file.Name}");

            var data = DefaultConverter.PrepareProducts(products.Product.Select(x => (x.Manufacturer, x.Name)));

            logger.LogDebug($"Converted to {data.Count} 'prepared' items");

            if (data.Count > 0)
            {
                await marketplaceService.SendProductsAsync(data);
            }
        }
    }
}
