﻿namespace PharmPixel.Connectors.Xml1
{
    using System;
    using System.IO;
    using System.Xml;
    using System.Xml.Linq;
    using System.Xml.Schema;

    public static class XmlUtils
    {
        private static readonly Lazy<XmlSchemaSet> Schemas = new Lazy<XmlSchemaSet>(() =>
        {
            var assembly = typeof(XmlUtils).Assembly;

            var schemas = new XmlSchemaSet();
            using (var sr = assembly.GetManifestResourceStream("PharmPixel.Connectors.Xml1.PharmPixel.xsd"))
            {
                schemas.Add(null, XmlReader.Create(sr));
            }

            return schemas;
        });

        public static XDocument Load(FileInfo file)
        {
            using (var stream = file.OpenRead())
            {
                return Load(stream);
            }
        }

        public static XDocument Load(Stream xml)
        {
            var doc = XDocument.Load(xml);
            doc.Validate(Schemas.Value, null); // throws exception when validation error occurs
            return doc;
        }

        public static T Deserialize<T>(FileInfo file)
        {
            using (var stream = file.OpenRead())
            {
                return Deserialize<T>(stream);
            }
        }

        public static T Deserialize<T>(Stream xml)
        {
            var doc = Load(xml);
            var serializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
            using (var reader = doc.CreateReader())
            {
                return (T)serializer.Deserialize(reader);
            }
        }
    }
}
