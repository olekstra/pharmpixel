﻿namespace Microsoft.Extensions.DependencyInjection
{
    using System;
    using System.Collections.Generic;
    using Microsoft.Extensions.Configuration;
    using PharmPixel.Connectors;
    using PharmPixel.Connectors.Xml1;

    public static class Xml1ConnectorsServiceCollectionExtensions
    {
        public static IServiceCollection AddXml1Connectors(this IServiceCollection services, IConfigurationSection monitoringTaskConfigurationSection)
        {
            services.AddTask<INewFilesMonitoringTask>(_ => { });

            services.AddScoped<INewFilesMonitoringTask, SimpleNewFilesMonitoringTask>();

            services.Configure<SimpleNewFilesMonitoringOptions>(monitoringTaskConfigurationSection);

            services.PostConfigure<SimpleNewFilesMonitoringOptions>(o =>
            {
                o.Handlers = new List<(string pattern, Type handler)>
                {
                    ("products_*", typeof(ProductsProcessor)),
                    ("stock_*", typeof(StockProcessor)),
                    ("sales_*", typeof(SalesProcessor)),
                };
            });

            services.AddScoped<ProductsProcessor>();
            services.AddScoped<StockProcessor>();
            services.AddScoped<SalesProcessor>();

            return services;
        }
    }
}
