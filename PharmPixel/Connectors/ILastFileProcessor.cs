﻿namespace PharmPixel.Connectors
{
    /// <summary>
    /// Интерфейс является лишь "маркером" что обрабатывать нужно только последний файл.
    /// </summary>
    public interface ILastFileProcessor : IFileProcessor
    {
        // Nothing
    }
}
