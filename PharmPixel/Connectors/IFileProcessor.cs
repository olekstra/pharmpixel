﻿namespace PharmPixel.Connectors
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using PharmPixel.Marketplace;

    public interface IFileProcessor
    {
        Task ProcessFileAsync(FileInfo file, IMarketplaceService marketplaceService);
    }
}
