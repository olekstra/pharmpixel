﻿namespace PharmPixel.Connectors
{
    public enum FileProcessResult : byte
    {
        /// <summary>
        /// File processed successfully.
        /// </summary>
        OK = 0,

        /// <summary>
        /// File was skipped.
        /// </summary>
        Skipped = 1,

        /// <summary>
        /// Processing of file failed.
        /// </summary>
        Failed = 2,
    }
}
