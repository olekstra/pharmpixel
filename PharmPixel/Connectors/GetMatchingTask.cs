﻿namespace PharmPixel.Connectors
{
    using System;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Options;
    using RecurrentTasks;

    public class GetMatchingTask : IRunnable
    {
        private readonly Marketplace.IMarketplaceService marketplaceService;

        private readonly ProgramOptions programOptions;

        public GetMatchingTask(Marketplace.IMarketplaceService marketplaceService, IOptionsSnapshot<ProgramOptions> programOptions)
        {
            this.marketplaceService = marketplaceService;
            this.programOptions = programOptions.Value;
        }

        public async Task RunAsync(ITask currentTask, IServiceProvider scopeServiceProvider, CancellationToken cancellationToken)
        {
            if (currentTask != null)
            {
                // options may change
                currentTask.Options.Interval = programOptions.MatchUpdateInterval;
            }

            await marketplaceService.LoginAsync();
            await marketplaceService.GetAndSaveProductMatchesAsync();
        }
    }
}
