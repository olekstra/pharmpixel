﻿namespace PharmPixel.Connectors.Csv14
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using PharmPixel.Connectors.Csv14.Dto;
    using PharmPixel.Converters;
    using PharmPixel.Marketplace;

    public class ProductAndStockProcessor : ILastFileProcessor
    {
        private readonly ILogger logger;

        public ProductAndStockProcessor(ILogger<ProductAndStockProcessor> logger)
        {
            this.logger = logger;
        }

        public async Task ProcessFileAsync(FileInfo file, IMarketplaceService marketplaceService)
        {
            var list = default(List<ProductAndStock>);

            using (var reader = new StreamReader(file.FullName, Csv14Options.Encoding))
            {
                using (var csv = new CsvHelper.CsvReader(reader, Csv14Options.CsvConfiguration))
                {
                    list = csv.GetRecords<ProductAndStock>().ToList();
                    logger.LogDebug($"Loaded {list.Count} items from {file.Name}");
                }
            }

            // 1. Отправляем товары
            var data1 = DefaultConverter.PrepareProducts(list.Select(x => (x.Manufacturer, x.Name)));
            logger.LogDebug($"Converted to {data1.Count} 'prepared' product items");

            if (data1.Count > 0)
            {
                await marketplaceService.SendProductsAsync(data1);
            }

            // 2. Отправляем остатки
            var data2 = await DefaultConverter.PrepareStockAsync(
                list.Select(x => (x.Manufacturer, x.Name, x.Provider, x.Stock)),
                marketplaceService);

            logger.LogDebug($"Converted to {data2.Count} 'prepared' stock items");

            if (data2.Count > 0)
            {
                await marketplaceService.SendStockAsync(data2);
            }
        }
    }
}
