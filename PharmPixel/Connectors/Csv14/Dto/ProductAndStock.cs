﻿namespace PharmPixel.Connectors.Csv14.Dto
{
    using System;
    using System.Globalization;
    using CsvHelper.Configuration.Attributes;

    public class ProductAndStock
    {
        [Index(14)]
        public string Name { get; set; }

        [Index(16)]
        public string Manufacturer { get; set; }

        [Index(3)]
        public string Provider { get; set; }

        [Index(21)]
        public decimal Stock { get; set; }

        [Index(2)]
        public string DateString
        {
            get
            {
                return Date.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                Date = DateTime.ParseExact(value, "ddMMyyyy", CultureInfo.InvariantCulture);
            }
        }

        [Ignore]
        public DateTime Date { get; set; }
    }
}
