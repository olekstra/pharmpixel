﻿namespace PharmPixel.Connectors.Csv14
{
    using System;
    using System.Globalization;
    using System.Text;
    using CsvHelper.Configuration;

    public class Csv14Options
    {
        public static Configuration CsvConfiguration { get; } = new Configuration(new CultureInfo("ru-RU"))
        {
            Delimiter = ";",
            HasHeaderRecord = false,
            MissingFieldFound = (x, y, z) => { },
        };

        public static Encoding Encoding { get; } = Csv12.Csv12Options.Encoding;
    }
}
