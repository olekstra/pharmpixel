﻿namespace Microsoft.Extensions.DependencyInjection
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.Extensions.Configuration;
    using PharmPixel.Connectors;
    using PharmPixel.Connectors.Csv14;

    public static class Csv14ConnectorsServiceCollectionExtensions
    {
        public static IServiceCollection AddCsv14Connectors(this IServiceCollection services, IConfigurationSection monitoringTaskConfigurationSection)
        {
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

            services.AddTask<INewFilesMonitoringTask>(_ => { });

            services.AddScoped<INewFilesMonitoringTask, GentleNewFilesMonitoringTask>();

            services.Configure<GentleNewFilesMonitoringOptions>(monitoringTaskConfigurationSection);

            services.PostConfigure<GentleNewFilesMonitoringOptions>(o =>
            {
                o.Handlers = new List<(string pattern, Type handler)>
                {
                    ("*.zip", typeof(ZipProcessor)),
                    ("*.ost*", typeof(ProductAndStockProcessor)),
                    ("*.mov*", typeof(SalesProcessor)),
                };
            });

            services.AddScoped<ZipProcessor>();
            services.AddScoped<ProductAndStockProcessor>();
            services.AddScoped<SalesProcessor>();

            return services;
        }
    }
}
