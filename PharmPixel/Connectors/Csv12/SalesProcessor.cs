﻿namespace PharmPixel.Connectors.Csv12
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using PharmPixel.Connectors.Csv12.Dto;
    using PharmPixel.Converters;
    using PharmPixel.FiscalOperators;
    using PharmPixel.Marketplace;

    public class SalesProcessor : IFileProcessor
    {
        private readonly ILogger logger;

        private readonly IFiscalOperatorService fiscalOperatorService;

        public SalesProcessor(ILogger<SalesProcessor> logger, IFiscalOperatorService fiscalOperatorService)
        {
            this.logger = logger;
            this.fiscalOperatorService = fiscalOperatorService;
        }

        public async Task ProcessFileAsync(FileInfo file, IMarketplaceService marketplaceService)
        {
            var list = default(List<Sale>);

            using (var reader = new StreamReader(file.FullName, Csv12Options.Encoding))
            {
                using (var csv = new CsvHelper.CsvReader(reader, Csv12Options.CsvConfiguration))
                {
                    list = csv.GetRecords<Sale>().ToList();
                    logger.LogDebug($"Loaded {list.Count} sales from {file.Name}");
                }
            }

            var sales = list.Select(x =>
                new DefaultConverter.SaleData
                {
                    Name = x.Name,
                    Manufacturer = x.Manufacturer,
                    Provider = x.Provider,
                    PriceSellIn = x.PriceSellIn,
                    PriceSellOut = x.PriceSellOut,
                    Quantity = x.Quantity,
                    DateTime = x.Date,
                    Number = null,
                });

            var (valid, invalid) = await DefaultConverter.PrepareSalesAsync(sales, marketplaceService, fiscalOperatorService, true);

            logger.LogDebug($"Converted to {valid.Count} 'valid' and {invalid.Count} 'invalid' items");

            if (valid.Count > 0)
            {
                await marketplaceService.SendSalesValidAsync(valid);
            }

            if (invalid.Count > 0)
            {
                await marketplaceService.SendSalesInvalidAsync(invalid);
            }
        }
    }
}
