﻿namespace PharmPixel.Connectors.Csv12
{
    using System;
    using System.Globalization;
    using System.Text;
    using CsvHelper.Configuration;

    public class Csv12Options
    {
        public static Configuration CsvConfiguration { get; } = new Configuration(new CultureInfo("ru-RU"))
        {
            Delimiter = ";",
            HasHeaderRecord = true,
            MissingFieldFound = (x, y, z) => { },
        };

        public static Encoding Encoding { get; } = Encoding.GetEncoding(1251);
    }
}
