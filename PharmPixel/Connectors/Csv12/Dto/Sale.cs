﻿namespace PharmPixel.Connectors.Csv12.Dto
{
    using System;
    using System.Globalization;
    using CsvHelper.Configuration.Attributes;

    public class Sale
    {
        [Name("Drug_Name")]
        public string Name { get; set; }

        [Name("Drug_Producer_Name")]
        public string Manufacturer { get; set; }

        [Name("Supplier")]
        public string Provider { get; set; }

        [Name("Cena_Zak")]
        public decimal PriceSellIn { get; set; }

        [Name("Cena_Rozn")]
        public decimal PriceSellOut { get; set; }

        [Name("Quant")]
        public decimal Quantity { get; set; }

        [Name("D_DOK")]
        public string DateString
        {
            get
            {
                return Date.ToString("ddMMyyyy", CultureInfo.InvariantCulture);
            }

            set
            {
                Date = DateTime.ParseExact(value, "ddMMyyyy", CultureInfo.InvariantCulture);
            }
        }

        [Ignore]
        public DateTime Date { get; set; }
    }
}
