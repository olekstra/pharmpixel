﻿namespace PharmPixel.Connectors
{
    using System;
    using System.IO;
    using System.IO.Compression;
    using System.Threading.Tasks;
    using Microsoft.Extensions.Logging;
    using PharmPixel.Marketplace;

    public class ZipProcessor : IFileProcessor
    {
        private readonly ILogger logger;

        public ZipProcessor(ILogger<ZipProcessor> logger)
        {
            this.logger = logger;
        }

        public Task ProcessFileAsync(FileInfo file, IMarketplaceService marketplaceService)
        {
            logger.LogDebug($"Unpacking {file.Name}...");

            using (var zip = new ZipArchive(file.OpenRead(), ZipArchiveMode.Read))
            {
                foreach (var entry in zip.Entries)
                {
                    var newName = Path.Combine(file.DirectoryName, entry.Name);
                    entry.ExtractToFile(newName, true);
                    logger.LogInformation($"Unpacked '{entry.Name}' from {file.Name}");
                }
            }

            return Task.CompletedTask;
        }
    }
}
