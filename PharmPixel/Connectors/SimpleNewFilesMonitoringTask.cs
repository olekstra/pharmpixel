﻿namespace PharmPixel.Connectors
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using Microsoft.Extensions.DependencyInjection;
    using Microsoft.Extensions.Logging;
    using Microsoft.Extensions.Options;
    using PharmPixel.Marketplace;
    using RecurrentTasks;

    public class SimpleNewFilesMonitoringTask : IRunnable, INewFilesMonitoringTask
    {
        private readonly ILogger logger;

        private readonly SimpleNewFilesMonitoringOptions options;

        public SimpleNewFilesMonitoringTask(ILogger<SimpleNewFilesMonitoringTask> logger, IOptionsSnapshot<SimpleNewFilesMonitoringOptions> options)
        {
            this.logger = logger;
            this.options = options.Value;
        }

        public async Task RunAsync(ITask currentTask, IServiceProvider scopeServiceProvider, CancellationToken cancellationToken)
        {
            if (currentTask != null)
            {
                // options may change
                currentTask.Options.Interval = options.MonitoringInterval;
            }

            var (main, done) = GetDirectories(options);
            if (main == null)
            {
                return;
            }

            var markFileAsProcessed = new Action<FileInfo, FileProcessResult>((file, result) => MoveProcessedFile(file, result, done));

            await ProcessFilesAsync(main, markFileAsProcessed, scopeServiceProvider);
        }

        public (DirectoryInfo main, DirectoryInfo done) GetDirectories(SimpleNewFilesMonitoringOptions options)
        {
            var main = new DirectoryInfo(options.Folder);
            if (!main.Exists)
            {
                logger.LogError("Directory does not exist: " + main.FullName);
                return (null, null);
            }

            var done = main.CreateSubdirectory(options.DoneSubfolder);

            return (main, done);
        }

        public void MoveProcessedFile(FileInfo file, FileProcessResult result, DirectoryInfo destination)
        {
            var fileName = file.Name;
            var fileWithoutExtension = Path.GetFileNameWithoutExtension(fileName);
            var fileExtension = Path.GetExtension(fileName);

            var newFileName = fileWithoutExtension + "_" + result.ToString().ToUpperInvariant() + fileExtension;

            var destinationFile = Path.Combine(destination.FullName, newFileName);

            // удаляем старый файл, чтобы не упасть при Move
            File.Delete(destinationFile);

            file.MoveTo(destinationFile);
            logger.LogDebug($"{fileName} is {result}, moved to {destination}");
        }

        public async Task ProcessFilesAsync(DirectoryInfo directory, Action<FileInfo, FileProcessResult> markAsProcessed, IServiceProvider serviceProvider)
        {
            var filesCount = 0;

            var marketplaceService = serviceProvider.GetRequiredService<IMarketplaceService>();

            foreach (var handler in options.Handlers)
            {
                var files = directory.GetFiles(handler.pattern).OrderBy(x => x.CreationTime).ToList();
                filesCount += files.Count;

                if (files.Count > 0)
                {
                    logger.LogDebug($"Found {files.Count} files for pattern '{handler.pattern}'...");

                    var processor = (IFileProcessor)serviceProvider.GetRequiredService(handler.handler);

                    if (processor is ILastFileProcessor multiProcessor)
                    {
                        // пропускаем все файлы кроме последнего
                        foreach (var file in files.SkipLast(1))
                        {
                            markAsProcessed(file, FileProcessResult.Skipped);
                        }

                        // и удаляем их из списка
                        files.RemoveRange(0, files.Count - 1);
                    }

                    foreach (var file in files)
                    {
                        try
                        {
                            await processor.ProcessFileAsync(file, marketplaceService);
                            markAsProcessed(file, FileProcessResult.OK);
                        }
                        catch (Exception ex)
                        {
                            logger.LogError(ex, $"Failed to process file {file.Name}");
                            markAsProcessed(file, FileProcessResult.Failed);
                        }
                    }
                }
            }

            logger.Log(
                filesCount == 0 ? LogLevel.Debug : LogLevel.Information,
                $"{filesCount} files processed in {directory.FullName}");
        }
    }
}
