﻿namespace PharmPixel.Connectors
{
    using RecurrentTasks;

    /// <summary>
    /// Интерфейс-маркер для регистрации реального таска.
    /// </summary>
    public interface INewFilesMonitoringTask : IRunnable
    {
        // Nothing
    }
}
