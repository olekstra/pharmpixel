﻿namespace PharmPixel.Connectors
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public interface IConnector
    {
        Task ProcessFilesAsync(DirectoryInfo directory, Action<FileInfo, FileProcessResult> markAsProcessed);
    }
}
