﻿# Pharm Pixel - Sooplata

## Среда разработки

1. Microsoft .NET Core 2.1 **SDK**

## Установка и запуск

1. Установить Microsoft .NET Core 2.1 Runtime
   * Если уже установлен SDK, то данный пункт можно пропустить;
   * Если уже установлена более поздняя версия (2.2, 3.0...), то данный пункт можно пропустить;
   * Проверить какая установлена версия можно выполнив в консоли команду `dotnet --version`: она выведет текущую установленую версию, например `3.0.100-preview6-012264` - первые два разряда (`3.0`) это и есть версия, если она больше или равна `2.1` то ничего доустанавливать не нужно;
   * Системные требования: Windows 7.1 SP1+, Windows 8.1, Windows 10, macOS, Linux ([см. подробности](https://github.com/dotnet/core/blob/master/release-notes/2.1/2.1-supported-os.md));
   * Скачивать с [dotnet.microsoft.com](https://dotnet.microsoft.com/download)
   * Lifecycle policy: [LTS, till August 2021](https://dotnet.microsoft.com/platform/support/policy/dotnet-core)
2. Распаковать архив в произвольную папку;
3. Скопировать файл `_settings_sample.json` в файл `_settings.json`, отредактировать файл `_settings.json` вписав значения настроек согласно комментариям.